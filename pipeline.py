#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

Pipeline.

Relation table must not have duplicate. (same transcription factor - gene relation)

- for modules :
action = "store_true"

- files input : 
widget = FileChooser
widget = DirChooser (fichier de résulats)

- files output : 
widget = FileSaver

- int : 
widget = IntegerField
widget = DecimalField 

- names
widget = TextCtrl, action = "store"

- colour : 
widget = ColourChooser

https://github.com/chriskiehl/Gooey/blob/master/docs/Gooey-Options.md 
https://htmlcolorcodes.com/


markdown : https://iunctis.fr/t/codes-de-mise-en-forme-markdown-html-bbcode/3168

[su]Part_1 :[/su]

<font color=red>Part_1</font>

[size=7]Part_1[/size]

[center]Part_1[center]

https://github.github.com/gfm/#example-203

tests : 
https://docs.python-guide.org/writing/tests/


Uses GUI gooey : graĥical user interface 


git add README.md; git add compares_TF.py; git add get_annot_tf.py; git add get_gene_pattern_per_TF_graph.py; git add get_MeSH.py; git add get_targets_regulators.py; git add get_TF_pattern_graph.py; git add get_gene_pattern_per_TF_pattern_graph.py; git add graph_go.py; git add group_pattern.py; git add pipeline.py; git add relations_table_analysis.py; git add select_tf.py; git add setup.py; git add update_class.py

'''

from SPARQLWrapper import SPARQLWrapper, JSON
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
from Bio import Entrez, Medline
import argparse 
import time 
import datetime 
import matplotlib.pyplot as plt
import pandas as pd
from gooey import Gooey, GooeyParser, options
import os 
import sys
# import nbformat
# import nbconvert
import pickle
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast 

# # Or add to the pythonpath while download the package
# parser = argparse.ArgumentParser()

# parser.add_argument("-path", "--package_directory", help = "Path to the directory containing packages' scripts.")

# args_pipeline = parser.parse_args()

# # Checks existence of input files : 
# if not os.path.exists(args_pipeline.package_directory):
#     raise ValueError(f"{args_pipeline.package_directory} does not exist. Search where.")

# # Adds package directory as a pythonpath in order to allow scripts import 
# sys.path.insert(0, args_pipeline.package_directory)


# funcitons neeeded : 
def makes_intervals(table_data, col_name:str, nb_intervals:int) : 
    '''
    table_data must have only one columns
    '''

    table_data[col_name] = table_data[col_name].astype(int)

    maximum = int(table_data[[col_name]].max())
    minimum = int(table_data[[col_name]].min())
    step = ((maximum - minimum) / nb_intervals)

    intervals = []
    TFs_list = []

    table_data["TF_nb"] = table_data["Transcription_Factor"].astype(str) + "_" + table_data[col_name].astype(str)

    interval = minimum

    for coef in range(nb_intervals) : 

        intervals.append(f"[ {int(interval)}; {int(interval + step)} [")

        selected_rows = table_data[(interval < table_data[col_name]) & (table_data[col_name] < (interval + step))]
        TFs_list.append(", ".join(list(selected_rows["TF_nb"])))

        interval += step 

    return pd.DataFrame({col_name : intervals, "TFs" : TFs_list})


def stat(table_data:str, columns_to_analyse:str, types_graph:str, sep_table:str, graphic_dir:str) :
    '''
    '''

    #table_data = pd.read_csv(table_data_path, sep = sep_table, header = 0, engine = "python")


    if not os.path.exists(graphic_dir) :
        os.makedirs(graphic_dir)

    columns_to_analyse = columns_to_analyse.split(", ") 
    types_graph = types_graph.split(", ")

    if "hist" in types_graph :
        table_data[[columns_to_analyse[0]]].astype(float).plot(kind = "hist", figsize = (10,10), legend = True, title = f"{columns_to_analyse[0]}")
        plt.savefig(f"{graphic_dir}/{columns_to_analyse[0]}_hist.png")
        plt.close()

    if "box" in types_graph :
        table_data[[columns_to_analyse[0]]].astype(float).plot(kind = "box", figsize = (10,3), legend = False, title = f"{columns_to_analyse[0]}", vert = False)
        # , xticks = (table_data[[column]].describe()[(table_data[[column]].describe().index != "count") & (table_data[[column]].describe().index != "std")][column]))            
        plt.savefig(f"{graphic_dir}/{columns_to_analyse[0]}_box.png")
        plt.close()

    if "scatter" in types_graph :
        table_data.plot(kind = "scatter", x = columns_to_analyse[0], y = columns_to_analyse[1], figsize = (10,10), legend = True, title = f"{columns_to_analyse}")
        plt.savefig(f"{graphic_dir}/{columns_to_analyse[0]}_{columns_to_analyse[1]}_scatter.png")
        plt.close()

    if "bar" in types_graph :
        table_data.plot(kind = "bar", x = columns_to_analyse[0], y = columns_to_analyse[1], figsize = (10,10), legend = True, title = f"{columns_to_analyse}")
        plt.savefig(f"{graphic_dir}/{columns_to_analyse[0]}_{columns_to_analyse[1]}_bar.png")
        plt.close()

    if "line" in types_graph :
        table_data.plot(kind = "line", x = columns_to_analyse[0], y = columns_to_analyse[1], figsize = (10,10), legend = True, title = f"{columns_to_analyse}")
        plt.savefig(f"{graphic_dir}/{columns_to_analyse[0]}_{columns_to_analyse[1]}_line.png")
        plt.close()

def display_table_stat(hist_path:str, hist_name:str, box_path:str, box_name:str, describe_table_md) :
    '''
    '''

    text_info = f"""
<table>
    <thead>
        <tr>
            <th>Histogram</th>
            <th>Boxplot</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4><img src={hist_path} alt={hist_name} width="500"/></td>
            <td rowspan=2><img src={box_path}  alt={box_name}  width="500"/></td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td rowspan=2>

{describe_table_md}


</td>
        </tr>
    </tbody>
</table>
"""

    return text_info


def is_similar(pattern_1:str, pattern_2:str, distance_max:int) -> (bool, int) :
    '''
    Careful if patterns have regulatory direction added.
    Patterns must be integers.
    The maximum distance is the sum of difference between each patterns' digit 
    (digit of same position comparison)
    To know id patterns are opposed, switch on pattern for its opposed.
    Use get_opposed(pattern:str, max_digit:int, min_digit:int) function.
    '''

    if len(str(int(float(pattern_1)))) != len(str(int(float(pattern_2)))) :
        raise ValueError(f"Patterns to compare must have same length : {len(pattern_1)} != {len(pattern_2)}.")
    
    if type(distance_max) != int : 
        raise ValueError(f"The maximum distance between both patterns must be an integer.")
    distance = 0

    for (digit_1, digit_2) in zip(pattern_1, pattern_2) :
        digit_dist = abs(int(digit_1) - int(digit_2))
        distance += digit_dist
        if distance > distance_max : 
            return (False, distance) 
    return (True, distance)


def get_opposed(pattern:str, max_digit:int, min_digit:int) :
    '''
    Returns patterns' opposed pattern.

    Arguments : 
        pattern (str) : the pattern to get its opposed
        max_digit (int) : the maximum digit authorized in any patterns
        min_digit (int) : the minimum digit authorized in any patterns
    '''

    max_digit = int(max_digit)
    min_digit = int(min_digit)

    if max_digit < min_digit : 
        raise ValueError(f"The maximum digit ({max_digit}) must be superior than the minimum digit ({min_digit}).")
    
    if ((max_digit > 9) | (min_digit > 9)) :
        raise ValueError(f"Patterns' digit must be digit (0 to 9 only).")

    sum_digit = max_digit + min_digit
    opposed = ""

    for digit in str(int(float(pattern))) :
        opposed += str(sum_digit - int(digit))

    return str(opposed)


# def pipeline(columns_select_tf, relations_table_analysis, max_digit, min_digit, max_distance, follow_process_path, gene_pattern_per_TF_pattern_graph_dir, result_path_2, result_path_1, all_TF_table_annotated, TF_table_annotated_file, work_directory, graph_go, graph_go_file, go_terms_graph, graph_MeSH, MeSH_terms, get_MeSH, pmid_interest_file_path, output_mesh_file, TF_table_MeSH, max_pmid_per_TF_nb, is_uri, get_annot_tf_1, C_get_annot_tf, go_interest, unwanted_keywords, categories_keywords, A_group_pattern, output_count_table, output_relations_group_file, max_genes_per_pattern_number, Relations_table, Gene_to_get_rel, B_select_tf, threshold_type, percentage_threshold_cov_spe, preselected_table, columns_to_conserve, stores_Relations_table_file, stores_table_TF_file, genes_per_gene_pattern_count_table, get_rel_tf_1, get_rel_tf_3, get_gene_pattern_per_TF_pattern_graph, Transcription_Factors_interest, gene_pattern_per_TF_graph_dir, get_TF_pattern_graph, get_gene_pattern_per_TF_graph, TF_pattern_graph_file, Transcription_Factor_patterns, compares_TF, TF_table_1, TF_table_2, comparison_name, output_file_compares_TF, update_class, all_initial_TF, class_table, patterns_length, sep_table, max_MeSH_terms, min_MeSH_terms, get_G_citations_nb, delete_small_gene_patterns, gene_patterns_to_delete) :
def pipeline(args_ini, args, categories_keywords, columns_select_tf, cit_as_columns_class) :
    '''
    '''

    # # Checks if input file exists before to run modules : 
    # for input_file in ["Relations_table", "genes_per_gene_pattern_count_table", "pmid_interest_file_path", "TF_table_MeSH", "already_annotation_table", "TF_table_1", "TF_table_2", "all_initial_TF"] :
    #     if input_file in args :
    #         if (args[input_file] != False) & (args[input_file] != None) : 
    #             if not os.path.exists(args[input_file]) :
    #                 raise ValueError(f"Input file '{args[input_file]}' does not exist, please change our input and assert file exist and path correctly written.")
    
    # Variables
    t0 = time.time()
    executions_times = {}
    dict_annot_columns_id = {}
    annotated_table_path = None 

    # Writes a markdown file to stores process data :
    text_info = (f"""


********************************************************************************
*****************   Pipeline lauched : {datetime.datetime.today()}   *****************
********************************************************************************

""")

    print(text_info)

    # part_1 : Data preprocessing and queries' preparations 

    text_info += f"""
***

# <font color=red>Part_1 :</font>

"""

    # stores process info 
    with open(args.follow_process_path, "a") as follow_process_file : 
        follow_process_file.write(text_info)

    # sep_table  = args_ini.sep_table[1:-1]  # Because double quotes, to see tabulations in gooey window

    # launchs selected modules : 

    if args_ini.relations_table_analysis_1 : 
        import relations_table_analysis
        executions_times["relations_table_analysis_1"] = relations_table_analysis.relations_table_analysis(args.Relations_table, args.genes_per_gene_pattern_count_table, args.sep_table, args.follow_process_path)

    if args_ini.get_annot_tf_1 : 
        import get_annot_tf
        if args.already_annotation_table :
            if not args.dict_annotations_columns_id : 
                raise ValueError(f"If reuse an annotation table, precise annotation' columns' identifiers numbers in dict_annotations_columns_id, given from a previous query in the follow process file.")
            else : 
                dict_annot_columns_id = args.dict_annotations_columns_id
                annotated_table_path = args.already_annotation_table
        (dict_annot_columns_id, annotated_table_path, executions_times["get_annot_tf_1"]) = get_annot_tf.get_annot_tf(args.email, args.all_TF_table_annotated, args.follow_process_path, args.Relations_table, args.sep_table, args.go_interest, args.unwanted_keywords, categories_keywords, annotated_table_path, dict_annot_columns_id, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb)

    if args_ini.update_class :
        import update_class
        if args.all_TF_annotated_table_path : 
            annotation_table_for_class = args.all_TF_annotated_table_path                 
        elif args_ini.get_annot_tf_1 :
            annotation_table_for_class = annotated_table_path
        else : 
            annotated_table_for_class = None 

        if (not args_ini.get_annot_tf_1) & (not args_ini.C_get_annot_tf) & (not args_ini.A_group_pattern) & (not args_ini.B_select_tf) :
            executions_times["update_class"] = update_class.update_class(args.follow_process_path, args.TF_just_filtered, args.filter_name, args.class_table, annotation_table_for_class, args.sep_table, args.go_terms_as_column, cit_as_columns_class)
        else : 
            executions_times["update_class"] = update_class.update_class(args.follow_process_path, args.all_initial_TF, "all_TF", args.class_table, annotation_table_for_class, args.sep_table, args.go_terms_as_column, cit_as_columns_class)
 
        #   add on step with TF list before to filter with compatibility table
        #   update_class(expressed_TF, "expressed_TF", class_table, annotated_table_path, dict_annot_columns_id)

            (sec, minu, mem, size) = update_class.update_class(args.follow_process_path, args.Relations_table, "compatibility_TF", args.class_table, annotation_table_for_class, args.sep_table, args.go_terms_as_column, cit_as_columns_class)
            executions_times["update_class"][0] += sec
            executions_times["update_class"][1] += minu
            executions_times["update_class"][2] += mem
            executions_times["update_class"][3] += size


    if args_ini.graph_go : 
        import graph_go
        executions_times["graph_go"] = graph_go.graph_go(args.follow_process_path, args.go_terms_graph, args.graph_go_file)
    # if args_ini.graph_MeSH : 
    #     import graph_MeSH
    #     executions_times["graph_MeSH"] = graph_MeSH.graph_MeSH(args.follow_process_path, args.MeSH_terms, args.MeSH_graph_dir)
   
    if args_ini.get_MeSH : 
        import get_MeSH
        executions_times["get_MeSH"] = get_MeSH.get_MeSH(args.email, args.follow_process_path, args.pmid_interest_file_path, args.output_mesh_file, args.TF_table_MeSH, args.max_pmid_per_TF_nb, args.is_uri, args.sep_table)

    # if args_ini.get_rel_tf_1 : 
    #     import get_rel_tf
    #     executions_times["get_rel_tf_1"] = get_rel_tf.get_rel_tf(args.follow_process_path, args.Relations_table, args.Gene_to_get_rel, args.result_path_1, args.sep_table)

    if args_ini.get_targets_regulators_1 : 
        import get_targets_regulators
        executions_times["get_targets_regulators_1"] = get_targets_regulators.get_targets_regulators(args.follow_process_path, args.Relations_table, args.Genes_1, args.result_path_1, args.sep_table)

    # part_2 : Calculations and selection = query

    text_info = f"""
***

# <font color=red>Part_2 :</font>

    """
    # stores process info 
    with open(args.follow_process_path, "a") as follow_process_file : 
        follow_process_file.write(text_info)

    if args_ini.A_group_pattern : 
        import group_pattern
        executions_times["group_pattern"] = group_pattern.group_pattern(args.max_digit, args.min_digit, args.follow_process_path, args.Relations_table, args.genes_per_gene_pattern_count_table, args.output_count_table, args.output_relations_group_file, args.max_genes_per_pattern_number, args.delete_small_gene_patterns, args.gene_patterns_to_delete, args.sep_table)

        if args_ini.update_class : 
            (sec, minu, mem, size) = update_class.update_class(args.follow_process_path, args.output_relations_group_file, "group/delete_gene_pattern_TF", args.class_table, annotation_table_for_class, args.sep_table, args.go_terms_as_column, cit_as_columns_class)

            executions_times["update_class"][0] += sec
            executions_times["update_class"][1] += minu
            executions_times["update_class"][2] += mem
            executions_times["update_class"][3] += size

    if args_ini.B_select_tf :
        import select_tf

        if args_ini.A_group_pattern :
            executions_times["select_tf"] = select_tf.select_tf(args.follow_process_path, args.output_relations_group_file, args.stores_Relations_table_file, args.stores_table_TF_file, args.output_count_table, args.threshold_type, args.percentage_threshold_cov_spe, args.preselected_table, columns_to_conserve, "", args.sep_table)


        else : 
            executions_times["select_tf"] = select_tf.select_tf(args.follow_process_path, args.Relations_table, args.stores_Relations_table_file, args.stores_table_TF_file, args.genes_per_gene_pattern_count_table, args.threshold_type, args.percentage_threshold_cov_spe, args.preselected_table, columns_to_conserve, args.gene_patterns_to_delete, args.sep_table)


        P3_Relations_table = args.stores_Relations_table_file

        if args_ini.update_class : 
            (sec, minu, mem, size) = update_class.update_class(args.follow_process_path, args.stores_table_TF_file, "selected_cov/spe_TF", args.class_table, annotation_table_for_class, args.sep_table, args.go_terms_as_column, cit_as_columns_class)

            executions_times["update_class"][0] += sec
            executions_times["update_class"][1] += minu
            executions_times["update_class"][2] += mem
            executions_times["update_class"][3] += size

    elif (args_ini.get_targets_regulators_1 | args_ini.A_group_pattern | args_ini.B_select_tf | args_ini.get_annot_tf_1 | args_ini.C_get_annot_tf | args_ini.relations_table_analysis_1 | args_ini.relations_table_analysis_3) :
        P3_Relations_table = args.Relations_table

    if args_ini.C_get_annot_tf : 

        # if already annotation table and not get annot in the first part, use
        if not args_ini.get_annot_tf_1 :
            if args.already_annotation_table :
                if not args.dict_annotations_columns_id : 
                    raise ValueError(playsound(args.beep, 1), f"If reuse an annotation table, precise annotation' columns' identifiers numbers in dict_annotations_columns_id, given from a previous query in the follow process file.")
                dict_annot_columns_id = args.dict_annotations_columns_id
                annotated_table_path = args.already_annotation_table

        if args_ini.get_annot_tf_1 :
            import get_annot_tf

        if args_ini.B_select_tf :
            if args.stores_table_TF_file : 
                (dict_annot_columns_id, annotated_table_path, execution_time) = get_annot_tf.get_annot_tf(args.email, args.TF_table_annotated_file, args.follow_process_path, args.stores_table_TF_file, args.sep_table, args.go_interest, args.unwanted_keywords, categories_keywords, annotated_table_path, dict_annot_columns_id, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb)
    

            elif args.stores_Relations_table_file :
                (dict_annot_columns_id, annotated_table_path, execution_time) = get_annot_tf.get_annot_tf(args.email, args.TF_table_annotated_file, args.follow_process_path, args.stores_Relations_table_file, args.sep_table, args.go_interest, args.unwanted_keywords, categories_keywords, annotated_table_path, dict_annot_columns_id, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb)
    

        elif args_ini.A_group_pattern : 
            (dict_annot_columns_id, annotated_table_path, execution_time) = get_annot_tf.get_annot_tf(args.email, args.TF_table_annotated_file, args.follow_process_path, args.output_relations_group_file, args.sep_table, args.go_interest, args.unwanted_keywords, categories_keywords, annotated_table_path, dict_annot_columns_id, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb)


        elif not args_ini.get_annot_tf_1 :   
            (dict_annot_columns_id, annotated_table_path, execution_time) = get_annot_tf.get_annot_tf(args.email, args.TF_table_annotated_file, args.follow_process_path, args.Relations_table, args.sep_table, args.go_interest, args.unwanted_keywords, categories_keywords, annotated_table_path, dict_annot_columns_id, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb)


        if args_ini.get_annot_tf_1 :
            executions_times["get_annot_tf_2"][0] += execution_time[0]
            executions_times["get_annot_tf_2"][1] += execution_time[1]
            executions_times["get_annot_tf_2"][2] += execution_time[2]
            executions_times["get_annot_tf_2"][3] += execution_time[3] 

        else : 
            executions_times["get_annot_tf_2"] = execution_time
        

    # continue to select TF on their annotations : 
    # new module to select TF in their annotations and add a row in class_table 

    # part_3 : Analysis of resutls and display

    text_info = f"""
*** 

# <font color=red>Part_3 :</font>

"""

    # stores process info 
    with open(args.follow_process_path, "a") as follow_process_file : 
        follow_process_file.write(text_info)

    if args_ini.A_group_pattern : 
        if args_ini.relations_table_analysis_3 : 
            import relations_table_analysis
            executions_times["relations_table_analysis_3"] = relations_table_analysis.relations_table_analysis(args.output_relations_group_file, args.output_count_table, args.sep_table, args.follow_process_path)


    if args_ini.get_gene_pattern_per_TF_graph : 
        import get_gene_pattern_per_TF_graph
        executions_times["get_gene_pattern_per_TF_graph"] = get_gene_pattern_per_TF_graph.get_gene_pattern_per_TF_graph(args.max_digit, args.min_digit, args.max_distance, args.follow_process_path, args.TF_table_to_analyse_with_graphs, args.Transcription_Factors_interest, args.gene_pattern_per_TF_graph_dir, args.sep_table)

    if args_ini.get_gene_pattern_per_TF_pattern_graph : 
        import get_gene_pattern_per_TF_pattern_graph
        executions_times["get_gene_pattern_per_TF_pattern_graph"] = get_gene_pattern_per_TF_pattern_graph.get_gene_pattern_per_TF_pattern_graph(args.max_digit, args.min_digit, args.max_distance, args.follow_process_path, args.TF_table_to_analyse_with_graphs, args.Transcription_Factor_patterns, args.gene_pattern_per_TF_pattern_graph_dir, args.sep_table)

    if args_ini.compares_TF : 
        import compares_TF
        executions_times["compares_TF"] = compares_TF.compares_TF(args.follow_process_path, args.TF_table_1, args.TF_table_2, args.comparison_name, args.output_file_compares_TF, args.sep_table)

    if args_ini.get_TF_pattern_graph : 
        import get_TF_pattern_graph
        executions_times["get_TF_pattern_graph"] = get_TF_pattern_graph.get_TF_pattern_graph(args.max_digit, args.min_digit, args.max_distance, args.follow_process_path, args.TF_table_to_analyse_with_graphs, args.TF_pattern_graph_file, args.sep_table)

    # if args_ini.get_rel_tf_3 :
    #     import get_rel_tf
    #     executions_times["get_rel_tf_3"] = get_rel_tf.get_rel_tf(args.follow_process_path, P3_Relations_table, args.Gene_to_get_rel, args.result_path_2, args.sep_table)

    if args_ini.get_targets_regulators_3 : 
        import get_targets_regulators
        executions_times["get_targets_regulators_3"] = get_targets_regulators.get_targets_regulators(args.follow_process_path, P3_Relations_table, args.Genes_3, args.result_path_3, args.sep_table)

    print(f"Pipeline ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")

    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    executions_times["pipeline (all modules)"] = [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), sum([exe[3] for exe in executions_times.values()])]
    return executions_times


@Gooey(advanced = True, program_description = "This tool helps the analyse of a RDF regulatory network.\nSee README.md for more explanations : https://gitlab.com/EveBarre/stage_eve_2021",
menu = [
    {"name" : "Documentation links", "items" : [
        # types can be Link, AboutDialog or MessageDialog
        {"type" : "Link", "menuTitle" : "README", "url" : "https://gitlab.com/EveBarre/stage_eve_2021"},
        {"type" : "Link", "menuTitle" : "Modules diagram", "url" : "https://gitlab.com/EveBarre/stage_eve_2021/-/tree/master/Tool_descriptions"},
        {"type" : "Link", "menuTitle" : "About modules parameters", "url" : "https://gitlab.com/EveBarre/stage_eve_2021/-/tree/master/Tool_descriptions"},
        {"type" : "Link", "menuTitle" : "Results examples", "url" : "https://gitlab.com/EveBarre/stage_eve_2021/-/tree/master/Tool_descriptions"},
    ]},
    {"name" : "Modules part_1", "items" : [
        {"type" : "AboutDialog", "menuTitle" : "relations_table_analysis"},
        {"type" : "AboutDialog", "menuTitle" : "get_MeSH"},
        {"type" : "AboutDialog", "menuTitle" : "graph_go"},
        # {"type" : "AboutDialog", "menuTitle" : "graph_MeSH"},
        {"type" : "AboutDialog", "menuTitle" : "get_annot_tf_1"},
        {"type" : "AboutDialog", "menuTitle" : "get_rel_tf_1"}
        ]},
    {"name" : "Modules part_2", "items" : [
        {"type" : "AboutDialog", "menuTitle" : "A_group_pattern"},
        {"type" : "AboutDialog", "menuTitle" : "B_select_tf"},
        {"type" : "AboutDialog", "menuTitle" : "C_get_annot_tf"}
        ]},
    {"name" : "Modules part_3", "items" : [
        {"type" : "AboutDialog", "menuTitle" : "get_rel_tf_3"},
        {"type" : "AboutDialog", "menuTitle" : "get_TF_pattern_graph"},
        {"type" : "AboutDialog", "menuTitle" : "get_gene_pattern_per_TF_graph"},
        {"type" : "AboutDialog", "menuTitle" : "get_gene_pattern_per_TF_pattern_graph"},
        {"type" : "AboutDialog", "menuTitle" : "update_class"},
        {"type" : "AboutDialog", "menuTitle" : "compares_TF"}
        ]},
    {"name" : "About", "items" : [
        {"type" : "AboutDialog", "menuTitle" : "About us"},
        {"type" : "AboutDialog", "menuTitle" : "About the tool"},
    
    ]}
    # adds for each modules 

], default_size = (2000, 1000))
def modules_arguments(args_ini) :  # , graph_MeSH
    '''
    Modules_arguments() arguments can also be args_ini but requires to replace args_ini before variables 
    Print dialog gooey window to defines modules selected parameters.
    Creates directeories needed. But don't launches modules.
    Documentation on modules available on the gooey dialag window. 

    defines general and mandatory argument in the first window (directory)
    '''
    
    parser = GooeyParser()

    general_arguments = parser.add_argument_group("General_arguments", gooey_options = {"show_border" : False})
    # general_arguments.add_argument("-cn", "--constant_digit", help = "Digit representing constant profile. (Ex : 5)", widget = "IntegerField", default = 5, gooey_options = {"min" : 0, "max" : 9})
    general_arguments.add_argument("--follow_process_path", help = "Path to file storing following process data.", widget = "FileChooser", default = f"{args_ini.work_directory}/Results_analysis/Follow_process.md", gooey_options = {'validator' : {'test' : 'user_input[-3:] == ".md"', 'message': 'It must be a markdown file.'}})
    general_arguments.add_argument("--beep", help = "  If do a beep when a module and the query is finished.", action = "store_true")
    general_arguments.add_argument("-s", "--sep_table", help = "String that separate columns in tables.", default = "\t")
 
    # Specific general arguments : 
    if (args_ini.get_gene_pattern_per_TF_graph | args_ini.get_gene_pattern_per_TF_pattern_graph | args_ini.get_TF_pattern_graph) :
        general_arguments.add_argument("--max_distance", help = "Maximum distance between two pattern considered as similar.", widget = "IntegerField", default = 1, gooey_options = {'min' : 1, 'max' : 100})
        
    if (args_ini.A_group_pattern | args_ini.get_gene_pattern_per_TF_graph | args_ini.get_gene_pattern_per_TF_pattern_graph | args_ini.get_TF_pattern_graph) :
        general_arguments.add_argument("--min_digit", help = "The minimum digit authorized in patterns.", widget = "IntegerField", default = 1, gooey_options = {"min" : 0, "max" : 8})
        general_arguments.add_argument("--max_digit", help = "The maximum digit authorized in patterns.", widget = "IntegerField", default = 4, gooey_options = {"min" : 1, "max" : 9})

    if (args_ini.get_targets_regulators_1 | args_ini.A_group_pattern | args_ini.B_select_tf | args_ini.get_annot_tf_1 | args_ini.C_get_annot_tf | args_ini.relations_table_analysis_1 | args_ini.relations_table_analysis_3) :  # | args_ini.get_rel_tf_3 | args_ini.get_rel_tf_1 ) :
        general_arguments.add_argument("--Relations_table", help = "Path to the relation (TF-gene) table.", widget = "FileChooser", default = "/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/scripts_tests_brouillons/test_dir_0_5/Relations_tables/Cell_B_rel_test_0_5.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})  # f"{work_directory}/Relations_tables/my_Relations_table.csv"
        
    if (args_ini.relations_table_analysis_1 | args_ini.relations_table_analysis_3 | args_ini.A_group_pattern | args_ini.B_select_tf) : 
        general_arguments.add_argument("-gct", "--genes_per_gene_pattern_count_table", help = "Path to the file containing the count of genes per gene's pattern.", widget = "FileChooser", default = "/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/scripts_tests_brouillons/test_dir_0_5/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table_test_0_5.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})  # f"{work_directory}/genes_per_gene_pattern_count_tables/genes_per_gene_pattern_count_table.csv"
  
    if (args_ini.A_group_pattern | args_ini.B_select_tf) :
        general_arguments.add_argument("--gene_patterns_to_delete", help = "Deletes genes_pattern not interesting (as constant patterns) given.", gooey_options = {"placeholder" : "5555"}) #, gooey_options = {"test" : f"len(user_input) == {int(args_ini.patterns_length)}", "message" : f"Theses patterns must have the length informed {args_ini.patterns_length}."})


    # part_1 : Data preprocessing and queries' preparations 
    part_1 = parser.add_argument_group("Part_1", description = "Data preprocessing and queries' preparations\nSee modules' description in menu bar 'Modules part_1' that helps parameters choice.")
    part_1.add_argument("-Change_part_1_name")

    text = ("""


Modules selected :

""")
    estimated_query_time = 0

    if args_ini.relations_table_analysis_1 :
        estimated_query_time += 0
        text += ("- relations_table_analysis_1 (0 minutes seconds)\n")

    if args_ini.graph_go : 
        estimated_query_time += 0
        text +=("- graph_go (0 minutes seconds)\n")

        graph_go_group = part_1.add_argument_group("graph_go", gooey_options = {'show_border': True})
        graph_go_group.add_argument("--go_terms_graph", help = "Label and gene ontology identifer for the one the parent graph is wanted.\nSearch gene ontology identifiers on <http://amigo.geneontology.org/amigo>.", gooey_options = {"placeholder" : "0002317, 0019724"})  # , "validator" : {"test" : "all([go.isdigit() for go in user_input.split(', ')])"}})
        graph_go_group.add_argument("--graph_go_file", help = "Path to the graph's file.", widget = "FileSaver", default = f"{args_ini.work_directory}/Results_analysis/graphs/go_graphs/my_go_graph.png", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".png"', 'message': 'Table must be a .png file.'}})
    
    # see in https://www.ncbi.nlm.nih.gov/mesh/ to have MeSH hierarchy

    # if graph_MeSH : 
    #     estimated_query_time += 0
    #     text +=("- graph_MeSH (0 minutes seconds)\n")
    #     graph_MeSH_group = part_1.add_argument_group("graph_MeSH", gooey_options = {'show_border': True})
    #     graph_MeSH_group.add_argument("-MeSH", "--MeSH_terms", help = "Labels and MeSH identifers to provide the related graph.", gooey_options = {"validator" : {}, "placeholder" : "B-Lymphocytes:D001402, Cell Differentiation:D002454"})
    #     graph_MeSH_group.add_argument("-file", "--MeSH_graph_dir", help = "Path to the directory storing MeSH terms graphs.", widget = "DirChooser", default = f"{work_directory}/Results_analysis/graphs/MeSH_graphs")

    if args_ini.get_MeSH : 
        estimated_query_time += 0
        text +=("- get_MeSH (0 minutes seconds)\n")
        get_MeSH_group = part_1.add_argument_group("get_MeSH", description = "Provides MeSH terms from a pmid list **OR** a transcription factors table given.", gooey_options = {'show_border': True})
        get_MeSH_group.add_argument("--pmid_interest_file_path", help = "Path to the file containing the table of pmid of interest. (in column)", widget = "FileChooser", gooey_options = {'placeholder' : f"{args_ini.work_directory}/Results_analysis/pmid_interest.csv", 'validator' : {'test' : '((user_input[-4:] == ".csv") | (user_input[-4:] == ".txt") | (user_input == None))', 'message': 'The table must exist and be a .csv or a .txt file.'}})
        get_MeSH_group.add_argument("--output_mesh_file", help = "Path to the file containing the table of mesh count.", widget = "FileSaver", default = f"{args_ini.work_directory}/Results_analysis/MeSH_terms/my_mesh_file.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})
        get_MeSH_group.add_argument("--TF_table_MeSH", help = "Path to the file containing the table of transcription factors.", widget = "FileChooser", gooey_options = {'placeholder' : f"{args_ini.work_directory}/TF_tables/interest_TF_list.csv", 'validator' : {'test' : '((user_input[-4:] == ".csv") | (user_input[-4:] == ".txt") | (user_input == None))', 'message': 'The table must exist and be a .csv file.'}})  #  default = f"{work_directory}/TF_tables/my_TF_table.csv",
        get_MeSH_group.add_argument("--max_pmid_per_TF_nb", help = "Maximum number of papers annotating a transcription factor.", widget = "IntegerField", default = 50, gooey_options = {"min" : 5, "max" : 500})
        get_MeSH_group.add_argument("-uri", "--is_uri", help = "  Bool, True if table of pmid given is a uri containing pmid list\nas 'https://pubmed.ncbi.nlm.nih.gov/12524387/'.", action = "store_true")
        if (not args_ini.get_annot_tf_1) & (not args_ini.C_get_annot_tf) :
            get_MeSH_group.add_argument("--email", help = "email address to informs who launch query to nbci (mandatory).", default = "eve.barre.1@etudiant.univ-rennes1.fr", gooey_options = {"placeholder" : "firstname.name@example.fr", "validator" : {"test" : "(user_input.count('@') == 1)"}})

    if args_ini.get_annot_tf_1 | args_ini.C_get_annot_tf : 
        estimated_query_time += 0
        text +=("- get_annot_tf (0 minutes seconds)\n")
        get_annot_tf_group = part_1.add_argument_group("get_annot_tf", gooey_options = {'show_border': True})
        get_annot_tf_group.add_argument("--email", help = "email address to informs who launch query to nbci (mandatory).", default = "eve.barre.1@etudiant.univ-rennes1.fr", gooey_options = {"placeholder" : "firstname.name@example.fr", "validator" : {"test" : "(user_input.count('@') == 1)", "message" : "email adress must have an '@'"}})
        get_annot_tf_group.add_argument("--go_interest", help = "String of go terms (label:id) of interest separated by ', '.", gooey_options = {"placeholder" : "transcription regulator activity:0140110, cellular developmental process:0048869, immune system process:0002376, lymphocyte activation:0046649, B cell activation:0042113, plasma cell differenciation:0002317, B cell differentiation:0030183", "validator" : {"test" : "(user_input.count(':') > 0) | (user_input == None)", "message" : "Label and identifiers must be separated by ':'"}})
        get_annot_tf_group.add_argument("-kw", "--unwanted_keywords", help = "Specific keywords that are not wanted, separated by ', '.", gooey_options = {"placeholder" : "DNA-binding, Repressor, Activator, Transcription regulation"})
        get_annot_tf_group.add_argument("-max_cit", "--max_MeSH_terms", help = "Maximum list of MeSH terms (labels) that papers must be annotated with to be counted as a condition query (OR, AND).\nHelp to choose with get_MeSH module.", gooey_options = {"placeholder" : "(Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation OR Germinal Center) AND (Transcription Factors OR Gene Expression Regulation) AND Cell Differentiation"})
        get_annot_tf_group.add_argument("-min_cit", "--min_MeSH_terms", help = "Maximum list of MeSH terms (labels) that papers must be annotated with to be counted as a condition query (OR, AND).\nHelp to choose with get_MeSH module.", gooey_options = {"placeholder" : "(B-Lymphocytes OR Plasma Cells)"})
        get_annot_tf_group.add_argument("-cit", "--get_G_citations_nb", help = "  Bool, if general papers' citation for this TF number must be return as a column.", action = "store_true")
        get_annot_tf_group.add_argument("-aan", "--already_annotation_table", help = "Path to the all transcription factor annotated table.", widget = "FileChooser", gooey_options = {'validator' : {'test' : '((user_input[-4:] == ".csv") | (user_input == None))', 'message': 'The table must exist and be a .csv file.'}})  # , default =  f"{work_directory}/TF_tables/my_annotated_table.csv"
        get_annot_tf_group.add_argument("--dict_annotations_columns_id", help = "Dictionnary of annotations columns number given by an other query (in the follow process file) to reuse annotations of a table.")

        if args_ini.get_annot_tf_1 : 
            get_annot_tf_group.add_argument("--all_TF_table_annotated", help = "Stores file's name's transcription factors annotated.", default = f"{args_ini.work_directory}/TF_tables/my_all_TF_annotated_table.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})

        if args_ini.C_get_annot_tf :
            get_annot_tf_group.add_argument("--TF_table_annotated_file", help = "Stores file's name's transcription factors annotated.", default = f"{args_ini.work_directory}/TF_tables/my_TF_list_annotated.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})


        categories_keywords_group = get_annot_tf_group.add_argument_group("categories_keywords", "Wanted categories' keywords annotating transcriptiin factors", gooey_options = {"show_border" : False})
        categories_keywords_group.add_argument("--Biological_process", action = "store_true")
        categories_keywords_group.add_argument("--Molecular_function", action = "store_true")
        categories_keywords_group.add_argument("--Cellular_component", action = "store_true")
        categories_keywords_group.add_argument("--Coding_sequence_diversity", action = "store_true")
        categories_keywords_group.add_argument("--Developmental_stage", action = "store_true")
        categories_keywords_group.add_argument("--Disease", action = "store_true")
        categories_keywords_group.add_argument("--Domain", action = "store_true")
        categories_keywords_group.add_argument("--Ligand", action = "store_true")
        categories_keywords_group.add_argument("--Post_translational_modification", action = "store_true")  # Normaly : Post-translational_modification
        categories_keywords_group.add_argument("--Technical_term", action = "store_true")
        # add one for the SPARQL endoint ?

    # if args_ini.get_rel_tf_1: 
    #     estimated_query_time += 0
    #     text +=("- get_rel_tf (0 minutes seconds)\n")
    #     # precise that is in the first part (differ from the last one) -> change arguments
    #     get_rel_tf_group = part_1.add_argument_group("get_rel_tf")
    #     get_rel_tf_group.add_argument("-g", "--Gene_to_get_rel", help = "Gene symbole name of gene of interest.", gooey_options = {"placeholder" : "PRDM1, BACH2, IRF4..."})
    #     get_rel_tf_group.add_argument("--result_path_1", help = "Path to the result when done in the forst preprocessed part.", default = f"{work_directory}/Relations_tables/my_result_file_1.csv")
    #     # add possible for a list of genes

    if args_ini.get_targets_regulators_1 : 
        get_targets_regulators_group_1 = part_1.add_argument_group("get_targets_regulators", gooey_options = {'show_border': True})
        get_targets_regulators_group_1.add_argument("--Genes_1", help = "Genes of interest.", gooey_options = {"placeholder" : "PRDM1, BACH_2, IRF4..."})
        get_targets_regulators_group_1.add_argument("--result_path_1", help = "Path to the file to save relations process selected.", default = f"{args_ini.work_directory}/Relations_tables/my_result_file_1.csv", widget = "FileChooser")


    # part_2 : Calculations and selection = query
    part_2 = parser.add_argument_group("part_2", description = "Calculations and selection = query\nSee modules' description in menu bar 'Modules part_2' that helps parameters choice.")
    part_2.add_argument("-Change_part_2_name")

    if args_ini.A_group_pattern : 
        estimated_query_time += 0
        text +=("- group_pattern (0 minutes seconds)\n")

        group_pattern_group = part_2.add_argument_group("group_pattern", gooey_options = {'show_border': True})
        group_pattern_group.add_argument("-or", "--output_relations_group_file", help = "Path to the output file (table containing transcription factor gathered by gene's pattern groups).", widget = "FileSaver", default = f"{args_ini.work_directory}/Relations_tables/my_output_relations_group_file.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'Table must be a .csv file.'}})
        group_pattern_group.add_argument("-oc", "--output_count_table", help = "Path to the new genes per genes' profiles count table.", widget = "FileSaver", default = f"{args_ini.work_directory}/genes_per_gene_pattern_count_tables/my_output_count_table.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'Table must be a .csv file.'}})
        group_pattern_group.add_argument("--max_genes_per_pattern_number", help = "The maximum number of gene per gene's pattern authorizted to allow group with the opposed pattern.", gooey_options = {"max" : 20000, "min" : 1, "placeholder" : "Enter the threshold number"})
        group_pattern_group.add_argument("--delete_small_gene_patterns", help = "Deletes instead of group gene's patterns with its opposed. Give the gene count threshold.", gooey_options = {"max" : 20000, "min" : 1, "placeholder" : "Enter the threshold number"})
   
    if args_ini.B_select_tf :
        estimated_query_time += 0
        text +=("- selected_tf (0 minutes seconds)\n")

        select_tf_group = part_2.add_argument_group("select_tf", gooey_options = {'show_border': True})
        select_tf_group.add_argument("--stores_Relations_table_file", help = "Path to the selected reltations' table file to store, if wanted to be returned.", widget = "FileSaver", gooey_options = {'placeholder' : f"{args_ini.work_directory}/Relations_tables/my_Relations_table_file.csv", 'validator' : {'test' : '(user_input[-4:] == ".csv") | (user_input == None)', 'message': 'Table must be a .csv file.'}})
        select_tf_group.add_argument("--stores_table_TF_file", help = "Path to the selected transcription factors file, if wanted to be returned.", widget = "FileSaver", gooey_options = {'placeholder' : f"{args_ini.work_directory}/TF_tables/my_table_selected_TF_file.csv", 'validator' : {'test' : '(user_input[-4:] == ".csv") | (user_input == None)', 'message': 'Table must be a .csv file.'}})
        select_tf_group.add_argument("-th", "--threshold_type", help = "The type of threshold can be :\n'mean+std' or a quantile (like : 'q85', 'q95'...)'", gooey_options = {"placeholder":"mean+std or a quantile (like : q50, q75, q95...)", "validator" : {"test" : "((user_input == 'mean+std') | ((user_input[0] == 'q') & (user_input[1:].isdigit())))"}}) 
        select_tf_group.add_argument("-percs", "--percentage_threshold_cov_spe", help= "Minimum percentage of TF's coverage and specificy for a gene_pattern in decimal to filter\nExamples : '0.75, 0.80', '0.75, No' <=> 'cov, spe'.", gooey_options = {"placeholder" : "cov, spe like 0.50, 0.95"})
        select_tf_group.add_argument("-presel", "--preselected_table", help= "Path to the preselected table but with data on the selection", widget = "FileSaver", gooey_options = {'placeholder' : f"path_to_my_preselected_table.csv", 'validator' : {'test' : '(user_input[-4:] == ".csv") | (user_input == None)', 'message': 'Table must be a .csv file.'}})
            
        columns_to_conserve = select_tf_group.add_argument_group("columns_to_conserve", "Specifies columns in the output table to concerve.")
        columns_to_conserve.add_argument("--Transcription_Factor", action = "store_true")
        columns_to_conserve.add_argument("--Pattern_TF", action = "store_true")
        columns_to_conserve.add_argument("--Nb_genes_per_TF", action = "store_true")
        columns_to_conserve.add_argument("--Nb_relations_selected", action = "store_true")
        columns_to_conserve.add_argument("--selected_pattern_nb", action = "store_true")
        columns_to_conserve.add_argument("--Patterns_targeted_pos", action = "store_true")
        columns_to_conserve.add_argument("--Patterns_targeted_neg", action = "store_true")
        columns_to_conserve.add_argument("--Patterns_targeted", action = "store_true")
        # add others ...
    
    # continue to select TF on their annotations : 
    # new module to select TF in their annotations and add a row in class_table 


    # part_3 : Analysis of resutls and display
    part_3 = parser.add_argument_group("part_3", description = "part_3 : Analysis of resutls and display\nSee modules' description in menu bar 'Modules part_3' that helps parameters choice.")
    part_3.add_argument("-Change_part_3_name")

    if (args_ini.get_gene_pattern_per_TF_graph | args_ini.get_gene_pattern_per_TF_pattern_graph | args_ini.get_TF_pattern_graph) :
        general_arguments.add_argument("--TF_table_to_analyse_with_graphs", help = "Path to the transcription factors' table to analyse.", widget = "FileChooser", default =  f"{args_ini.work_directory}/TF_tables/my_TF_table_to_analyse_with_graphs.csv")


    if args_ini.get_gene_pattern_per_TF_graph : 
        estimated_query_time += 0
        text += ("- get_gene_pattern_per_TF_graph (0 minutes seconds)\n")

        get_gene_pattern_per_TF_graph_group = part_3.add_argument_group("get_gene_pattern_per_TF_graph", gooey_options = {'show_border': True})
        get_gene_pattern_per_TF_graph_group.add_argument("--Transcription_Factors_interest", help = "Transcription factor for the one we want the pattern targeted graph, separated by ', '.", gooey_options = {"placeholder" : "Enter transcription factors names (ex : PRDM1, BACH2, IRF4, STAT6, ..."})
        get_gene_pattern_per_TF_graph_group.add_argument("--gene_pattern_per_TF_graph_dir", help = "Path to the graph's file.", widget = "DirChooser", default =  f"{args_ini.work_directory}/Results_analysis/graphs/gene_pattern_per_TF_graphs")  

    if args_ini.get_gene_pattern_per_TF_pattern_graph : 
        estimated_query_time += 0
        text += ("- get_gene_pattern_per_TF_pattern_graph (0 minutes seconds)\n")

        get_gene_pattern_per_TF_pattern_graph_group = part_3.add_argument_group("get_gene_pattern_per_TF_pattern_graph", gooey_options = {'show_border': True})
        get_gene_pattern_per_TF_pattern_graph_group.add_argument("--Transcription_Factor_patterns", help = "Transcription factor's patterns to provide graph, separated by ', '.", gooey_options = {"placeholder" : "Enter transcription factors patterns (ex : 4441, 1114, 1234...", "validator" : {"test" : "user_input != None", "message" : "Enter transcription factors patterns to save graphs."}})
        get_gene_pattern_per_TF_pattern_graph_group.add_argument("--gene_pattern_per_TF_pattern_graph_dir", help = "Path to the graph's file.", widget = "DirChooser", default =  f"{args_ini.work_directory}/Results_analysis/graphs/gene_pattern_per_TF_pattern_graphs")


    if args_ini.compares_TF : 
        estimated_query_time += 0
        text += ("- compares_TF (0 minutes seconds)\n")

        compares_TF_group = part_3.add_argument_group("Compares_TF", gooey_options = {'show_border': True})
        compares_TF_group.add_argument("-t1", "--TF_table_1", help = "Path to the first file containing Transcription factors to compare.", widget = "FileChooser", default = f"{args_ini.work_directory}/TF_tables/my_TF_table_1.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})
        compares_TF_group.add_argument("-t2", "--TF_table_2", help = "Path to the second file containing Transcription factors to compare.", widget = "FileChooser", default =  f"{args_ini.work_directory}/TF_tables/my_TF_table_2.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})
        compares_TF_group.add_argument("-cn", "--comparison_name", help = "Name to given to the comparison.", gooey_options = {"placeholder" :"Table_1_name vs Table_2_name"})
        compares_TF_group.add_argument("-oct", "--output_file_compares_TF", help = "Path to output file containing comparison.", widget = "FileSaver", default = f"{args_ini.work_directory}/Results_analysis/my_TF_comparison_file.md", gooey_options = {'validator' : {'test' : 'user_input[-3:] == ".md"', "message" : "pick folder"}})

 
    if args_ini.get_TF_pattern_graph : 
        estimated_query_time += 0
        text += ("- get_TF_pattern_graph (0 minutes seconds)\n")

        get_TF_pattern_graph_group = part_3.add_argument_group("get_TF_pattern_graph", gooey_options = {'show_border': True})
        get_TF_pattern_graph_group.add_argument("--TF_pattern_graph_file", help = "Path to the graph's file.", widget = "FileSaver", default = f"{args_ini.work_directory}/Results_analysis/graphs/gene_pattern_per_TF_graphs/my_TF_pattern_graph_file.png", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".png"', 'message': 'Table must be a .png file.'}})
        

    # if args_ini.get_rel_tf_3 :
    #     estimated_query_time += 0
    #     text += ("- get_rel_tf (0 minutes seconds)\n")

    #     # precise that is in the first part (differ from the last one) -> change arguments
    #     get_rel_tf_group = part_3.add_argument_group("get_rel_tf_3", gooey_options = {'show_border': True})
    #     get_rel_tf_group.add_argument("-g", "--Gene_to_get_rel", help = "Gene symbole name of gene of interest." )
    #     get_rel_tf_group.add_argument("--result_path_2", help = "Path to the result when done in the forst preprocessed part.", default = f"{work_directory}/Relations_tables/my_result_file.csv")
    #     # add possible for a list of genes


    if args_ini.get_targets_regulators_3 : 
        get_targets_regulators_group_3 = part_3.add_argument_group("get_targets_regulators", gooey_options = {'show_border': True})
        get_targets_regulators_group_3.add_argument("--Genes_3", help = "Gene symbole name of gene of interest." )
        get_targets_regulators_group_3.add_argument("--result_path_3", help = "Path to the file to save relations process selected.", widget = "FileChooser", default = f"{args_ini.work_directory}/Relations_tables/my_result_file_3.csv")


    if args_ini.update_class :
        text += ("- update_class (0 minutes seconds)\n ")

        estimated_query_time += 0
        update_class_group = part_3.add_argument_group("update_class", gooey_options = {'show_border': True})
        update_class_group.add_argument("--class_table", help = "Path to table that class all TF in function of filters used.", widget = "FileChooser", default = f"{args_ini.work_directory}/Results_analysis/class_table.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})
        update_class_group.add_argument("--go_terms_as_column", help = "Go terms to keep as column in the classification (gives the annoated TF count).", gooey_options = {"placeholder" : "transcription regulator activity, immune system process, ..."})
        citations_type = update_class_group.add_argument_group("citations_count_as_column", "Papers citation count (general, max, min) to keep as column in the classification (gives the annotated TF count).")
        citations_type.add_argument("--G_citations_nb", help = "  See TFs mean 'G_citations_nb' as a columns.", action = "store_true")
        citations_type.add_argument("--max_citations_nb", help = "  See TFs mean 'max_citations_nb' as a columns.", action = "store_true")
        citations_type.add_argument("--min_citations_nb", help = "  See TFs mean 'min_citations_nb' as a columns.", action = "store_true")

        if (not args_ini.get_annot_tf_1)  :
            update_class_group.add_argument("--all_TF_annotated_table_path", help = "Path to the annotated transcription factors table.", widget = "FileChooser", default = f"{args_ini.work_directory}/TF_table/TF_annotated_table.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})

            if (not args_ini.A_group_pattern) & (not args_ini.B_select_tf) & (not args_ini.C_get_annot_tf):
                update_class_group.add_argument("--filter_name", help = "Filter name applied on transcription factors.", gooey_options = {"placeholder" : "Enter Filter name."})           
                update_class_group.add_argument("--TF_just_filtered", help = "List of transcription factors just filtered.", widget = "FileChooser", default = f"{args_ini.work_directory}/TF_tables/TF_filtered.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})

        else : 
            update_class_group.add_argument("--all_initial_TF", help = "List of transcription factors known initially.", widget = "FileChooser", default = f"{args_ini.work_directory}/TF_tables/my_all_initial_TF.csv", gooey_options = {'validator' : {'test' : 'user_input[-4:] == ".csv"', 'message': 'The table must exist and be a .csv file.'}})

    text += (f"\nEstimated query spend time = {round(estimated_query_time/60, 2)} minutes.")
    print(text)
    print("\nChoose selected modules' arguments in the gooey's window just opened.")


    return (parser.parse_args(), text)

if __name__ == "__main__" :

    # to do a beep when query are finished : 
    try :
        import winsound
    except ImportError:
        def playsound(beep, type_sound):
            if not beep : 
                return None
            #apt-get install sox
            # os.system(f"play -n -t alsa synth 0.05 sine 400 vol 0.15 &> /dev/null; play -n -t alsa synth 0.05 sine 600 vol 0.15 &> /dev/null; play -n -t alsa synth 0.05 sine 800 vol 0.15 &> /dev/null") #; play -n -t alsa synth 0.3 sine 800 vol 0.1 &> /dev/null")
            if type_sound == 0 :
                os.system(f"play -n -t alsa synth 0.05 sine 400 vol 0.15; play -n -t alsa synth 0.05 sine 600 vol 0.15; play -n -t alsa synth 0.05 sine 800 vol 0.15 ") #; play -n -t alsa synth 0.3 sine 800 vol 0.1 &> /dev/null")
            if type_sound == 1 :
                os.system(f"play -n -t alsa synth 0.1 sine 300 vol 0.15; play -n -t alsa synth 0.1 sine 300 vol 0.15")
    else :
        def playsound(beep, type_sound):
            if not beep : 
                return None
            if type_sound == 0 :
                winsound.Beep(400,0.048)
                winsound.Beep(600,0.048)
                winsound.Beep(800,0.048)   
                winsound.Beep(800,0.3)    
            elif type_sound == 1 :     
                winsound.Beep(300,0.1)
                winsound.Beep(300,0.1)
    # load setup data (which module use)
    with open("setup.txt", "rb") as setup_file:
        args_ini = pickle.load(setup_file)

    # asks for moduels arguments 
    (args, text) = modules_arguments(args_ini)

    with open (args.follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text)

    # collecting checkbox arguments as a list : 
    columns_to_conserve = None
    if args_ini.B_select_tf :
        columns_to_conserve = []
        for (is_column, column) in zip([args.Transcription_Factor, args.Pattern_TF, args.Nb_genes_per_TF, args.Nb_relations_selected, args.selected_pattern_nb, args.Patterns_targeted_pos, args.Patterns_targeted_neg, args.Patterns_targeted], ["Transcription_Factor", "Pattern_TF", "Nb_genes_per_TF", "Nb_relations_selected", "selected_pattern_nb", "Patterns_targeted_pos", "Patterns_targeted_neg", "Patterns_targeted"]) :
            if is_column : 
                columns_to_conserve.append(column)
                #columns_to_conserve.extend([ k for k,v in locals().items() if v == columns][0][5:])  # returns variable's name
        columns_to_conserve = ", ".join(columns_to_conserve)

    categories_keywords = None
    if args_ini.get_annot_tf_1 | args_ini.C_get_annot_tf : 
        categories_keywords = []
        for (is_category, category) in zip([args.Biological_process, args.Cellular_component, args.Coding_sequence_diversity, args.Developmental_stage, args.Disease, args.Domain, args.Ligand, args.Molecular_function, args.Post_translational_modification, args.Technical_term], ["Biological process", "Cellular component", "Coding sequence diversity", "Developmental stage", "Disease", "Domain", "Ligand", "Molecular function", "Post-translational modification", "Technical term"]) :   # Normaly : Post-translational_modification
            if is_category : 
                categories_keywords.append(category)
                # categories_keywords.extend([ k for k,v in locals().items() if v == category][0][5:])  # returns variable's name
        categories_keywords = ", ".join(categories_keywords)

    cit_as_columns_class = None
    if args_ini.update_class :
        cit_as_columns_class = []
        for (is_cit, go_term) in zip([args.G_citations_nb, args.min_citations_nb, args.max_citations_nb], ["nb_G_citations", "nb_min_citations", "nb_max_citations"]) :
            if is_cit : 
                cit_as_columns_class.append(go_term)
        cit_as_columns_class = ", ".join(cit_as_columns_class)


    # execution_time = pipeline(columns_select_tf, args_ini.relations_table_analysis, args_ini.max_digit, args_ini.min_digit, args.max_distance, args.follow_process_path, args.gene_pattern_per_TF_pattern_graph_dir, args.result_path_2, args.result_path_1, args.all_TF_table_annotated, args.TF_table_annotated_file, args_ini.work_directory, args_ini.graph_go, args.graph_go_file, args.go_terms_graph, args_ini.graph_MeSH, args.MeSH_terms, args_ini.get_MeSH, args.pmid_interest_file_path, args.output_mesh_file, args.TF_table_MeSH, args.max_pmid_per_TF_nb, args.is_uri, args_ini.get_annot_tf_1, args_ini.C_get_annot_tf, args.TF_list_to_annot, args.go_interest, args.unwanted_keywords, categories_keywords, args_ini.A_group_pattern, args.output_count_table, args.output_relations_group_file, args.max_genes_per_pattern_number, args.Relations_table, args.Gene_to_get_rel, args_ini.B_select_tf, args.threshold_type, args.percentage_threshold_cov_spe, args.preselected_table, args.columns_to_conserve, args.stores_Relations_table_file, args.stores_table_TF_file, args.genes_per_gene_pattern_count_table, args_ini.get_rel_tf_1, args_ini.get_rel_tf_3, args_ini.get_gene_pattern_per_TF_pattern_graph, args.Transcription_Factors_interest, args.gene_pattern_per_TF_graph_dir, args_ini.get_TF_pattern_graph, args_ini.get_gene_pattern_per_TF_graph, args.TF_pattern_graph_file, args.Transcription_Factor_patterns, args_ini.compares_TF, args.TF_table_1, args.TF_table_2, args.comparison_name, args.output_file_compares_TF, args_ini.update_class, args.all_initial_TF, args.class_table, args_ini.patterns_length, args.sep_table, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb, args.delete_small_gene_patterns, args.gene_patterns_to_delete) 

    execution_time = pipeline(args_ini, args, categories_keywords, columns_to_conserve, cit_as_columns_class) 
    
    with open (args.follow_process_path, "a") as follow_process_file :
        follow_process_file.write("\nPipeline's execution time :\n\n")

        seconds = [exe_time[0] for exe_time in execution_time.values()]
        minutes = [exe_time[1] for exe_time in execution_time.values()]
        memory = [exe_time[2] for exe_time in execution_time.values()]
        input_size = [exe_time[3] for exe_time in execution_time.values()]
        tot_time = seconds[-1]
        tot_memory = memory[-1]

        query_analyse = pd.DataFrame({"modules" : [module for module in execution_time.keys()], "executions_times (sec)" : seconds, "executions_times (min)" : minutes, "memory usage (MiB)" : memory, "input_file_size (MiB)" : input_size})

        # adds execution time and memory usage percentages of each modules 
        query_analyse.insert((list(query_analyse.columns).index("executions_times (sec)") + 1), "executions_times (%)", round(((query_analyse["executions_times (sec)"] / tot_time ) * 100), 2))
        query_analyse.insert((list(query_analyse.columns).index("memory usage (MiB)") + 1), "memory usage (%)", round(((query_analyse["memory usage (MiB)"] / tot_memory ) * 100), 2))

        query_analyse.to_markdown(follow_process_file)
    
    playsound(args.beep, 0)