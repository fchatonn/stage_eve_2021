#! /usr/bin/env python
#-*- coding:utf-8 -*-


'''
@author : Ève Barré

This scripts adds colomns of transcription factors (TF) files that informs  
- if this TF is annotated gy a go term of interest. 
- In addition, provides the count of transcription factor's paper's citations 
- and uniprot keywords associated.

Don't research all annotation if there are already reserched for all transcription factors. 
In this case, argument -an must be used with the path to the table. 

The file of transcription factors of interest must have as first columns Gene symbol's of TFs. 

If not specified by the argument "-s", separator of input file is supposed to be tabulations "\t".

If not specified by argument "-go", go terms of interest are : /!\ with go terms length = 7
    - transcription regulator activity:0140110
    - cellular developmental process:0048869
    - immune system process:0002376
    - lymphocyte activation:0046649 (just added)
    - B cell activation:0042113
    - plasma cell differenciation:0002317.
    - B cell differentiation:0030183


Keywords not wanted by default are linked to general transcrition factor action function : 
    - DNA-binding
    - Repressor
    - Activator
    - Transcription regulation 


Keywords categories selected by default (on 10): 
    - Molecular function
    - Biological process
8 others not selected are :
    - Cellular component 
    - Coding sequence diversity
    - Developmental stage
    - Disease
    - Domain 
    - Ligand
    - Post-translational modification 
    - Technical term

The number of papers' citations for each TF are counted in tree conditions : https://www.ncbi.nlm.nih.gov/mesh
    - General : the count of all citing-TF given papers
    - Minimun annotated : the count of citing-TF papers annotated by the minimum of MeSH terms of interest given 
    (defines globaly the context, ex : Immunology)
        - Immunology : Q000276
"Immunology:Q000276"
The query : 
Immunology [Mesh] 
    - Maximum annotated : the count of citing-TF papers annotated by the maximum of MeSH terms of interest given 
    (the more precise, more close to the context of interest, ex : )
        - Cell Differentiation : D002454
        - Lymphocytes : D008214
        - Plasma Cell : D010950
        - B-Lymphocytes : D001402
        - ((& Transcription Factors : D014157))

"Cell Differentiation:D002454,Lymphocytes:D008214,Plasma Cell:D010950,B-Lymphocytes:D001402,Transcription Factors:D014157"

Endpoint requested : https://sparql.uniprot.org/sparql/

Process informations are written down on a file : args.follow_process_path.

Remarque : it is possible that gene encoded for several proteins.  

Ex : 
max => (Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation OR Germinal Center) AND (Transcription Factors OR Gene Expression Regulation) AND Cell Differentiation
min => (B-Lymphocytes OR Plasma Cells)

--> Arguments : 

"-tf", "--list_TF" : Path to the transcription factor selected by Regulus list.
"-s", "--sep_table" : String that separates columns in transcription table of interest
"-go", "--go_interest" : String of go terms of interest separated by ','. Example : 'cellular developmental process:0048869,immune system process:0002376,B cell activation:0042113,plasma cell differenciation:0002317'
"-kw", "--unwanted_keywords" : Specific keywords that are not wanted.
"-ckw", "--categories_keywords" : Wanted categories' keywords (as : Molecular funciton, Biological Process...)
"-aan", "--already_annotation_table" : Path to the all transcription factor annotated table.
"-max_cit", "--max_MeSH_terms" : Maximum list of MeSH terms (labels) that papers must be annotated with to be counted.
"-min_cit", "--min_MeSH_terms" : Maximum list of MeSH terms (labels) that papers must be annotated with to be counted.
"-dir", "--work_directory" : "Path to the directory to save result files.")

--> Proceeding : 

- write al along the script process data informations in a file 
    - script name
    - launch date and time 
    - arguments 
    - Statistics on numerics TF table datas
    - Number of TF's patterns per number of TF per TF's patterns
    - Table of number of TF with anntation and their labels
    - Keywords count on each keywords' category

- (tf) imports transcription factors list, with separation defined (s, default : "\t")
- (an) uses annotation table if already calculated from columns index given (col_cat, col_go, col_cit, col_G_cit, col_max_cit, col_min_cit)
- if not : search for each transcription factors : 
    - all paper citations count
    - paper citations with maximum MeSH terms of interest count (max_cit)
    - paper citations with minimum MeSH terms of interest count (min_cit)
    - if is annoted by go terms of interest (go)
    - keywords associated at a given category (ckw) and that aren't unwanted keyword (kw)
- adds transcription factors number (all) annotated by gene ontology terms of interest to add it in columns name
- calculated percentage of paper citations that are assocaited with MeSH terms of interest for each transcription factors
- stores transcription factors' annotation table 

--> A command example can be : 
python3 get_annot_tf.py -tf TF_tables/TF_table_q95.csv -s "\t" -max_cit "(Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation) AND Cell Differentiation AND Transcription Factors"  -min_cit "B-Lymphocytes"
python get_annot_tf.py -tf TF_tables/TF_table_meanstd.csv -s "\t" -an annotated_TF_table/all_TF_ini_annotated.csv -col_cat "3,4" -col_go "5,6,7,8,9,10"

--> memory usage : 

depends on : 

- get gereral citation number/min_MeSH_terms/max_MeSH_terms : Yes / No
- nb go terms of interest to request 
- if annotation already done 

'''

from SPARQLWrapper import SPARQLWrapper, JSON
from Bio import Entrez
import argparse
import pandas as pd
import time
import datetime
import os 
import matplotlib.pyplot as plt 
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast
import pipeline # to have stat and display_stat


# function needed : 

def get_TF_go_annot(email:str, Gene:str, list_go_interest:[], list_categories_keywords:[], list_unwanted_keywords:[], dict_annot_columns_id:{}, min_MeSH_terms:str, max_MeSH_terms:str, get_G_citations_nb:bool) -> [] :
    '''
    A function to inform if transcription factor of interest's is annotated by go terms of interest.

    Args : 
        Gene([]) : current gene name that encodes the transcription factor of interest.

    Warning, list_go_interest is previous by transcription factors count having this annotation (separated by '_')
    Returns:
        A list of keywords' label that annotate the transcription factor of interest.

    ''' 

    ## Counts of transcription factor citations

    Entrez.email = email

    col_annot = {}

    if get_G_citations_nb :
        ### All TF citations :
        handle = Entrez.egquery(term = (f"{Gene}"))
        record = Entrez.read(handle)
        for row in record["eGQueryResult"] :
            if row["DbName"] == "pubmed" :
                col_annot["nb_G_citations"] = row["Count"]

    ### MeSH minimum citations :
    # query_min = ""
    # for min_MeSH in min_MeSH_terms.split(", ") :
    #     query_min += f"{min_MeSH} [Mesh] OR"

    if min_MeSH_terms : 
        query = (min_MeSH_terms + f" AND {Gene}")
        handle = Entrez.egquery(term = query)  # f"Transcription Factors AND B-Lymphocytes AND {Gene}")
        record = Entrez.read(handle)
        for row in record["eGQueryResult"] :
            if row["DbName"] == "pubmed" :
                col_annot["nb_min_citations"] = row["Count"]

    ### MeSH maximum citations :
    # query_max = f"{Gene}"
    # for max_MeSH in max_MeSH_terms.split(", ")[1:] :
    #     query_max += f" AND {max_MeSH} [Mesh]"

    # handle = Entrez.egquery(term = (max_MeSH_terms + f"AND {Gene}"))  # (query_max))
    
    if max_MeSH_terms : 
        query = (max_MeSH_terms + f" AND {Gene}")
        handle = Entrez.egquery(term = query)  # f"(Plasma Cells OR B-Lymphocytes OR Lymphocyte Activation) AND Cell Differentiation AND Transcription Factors AND {Gene}")
        record = Entrez.read(handle)
        for row in record["eGQueryResult"] :
            if row["DbName"] == "pubmed" :
                col_annot["nb_max_citations"] = row["Count"]

    # Could be with uniprot but more precise with pubmed

    sparql = SPARQLWrapper("https://sparql.uniprot.org/sparql/")

    # sparql.setQuery("""
    #     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    #     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
    #     PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    #     PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
    #     PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
    #     PREFIX up: <http://purl.uniprot.org/core/>
    #     PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
    #     PREFIX go: <http://purl.obolibrary.org/obo/GO_>

    #     SELECT DISTINCT (COUNT(DISTINCT ?citation) AS ?nb_cit)

    #     WHERE {    
    #         VALUES ?gene_label {'""" + Gene + """'}  

    #         ?gene a up:Gene .
    #         ?gene skos:prefLabel ?gene_label .

    #         ?TF up:encodedBy ?gene .
    #         ?TF a up:Protein . 
    #         ?TF up:organism taxon:9606 .
            
    #         ?TF up:citation ?citation .
    #         ?citation rdf:type up:Journal_Citation .
        
    #     }

    # """)


    # sparql.setReturnFormat(JSON)
    # results = sparql.query().convert()

    # # Considers this number as int and not str in order to allow comparison on it ?
    # nb_cit = int([result["nb_cit"]["value"] for result in results["results"]["bindings"]][0]) 

    # if nb_cit == []:
    #     col_annot = {"nb_citation":0}

    # else : col_annot = {"nb_citation":nb_cit}


    if list_go_interest : 
        ## Tells if TF is annotated by go terms of interest :

        # Writes in sparql go terms of interest to filter : 
        filter_sparql_go = f"(regex(?go, go:{list_go_interest[0][-7:]}))"  # to have only GO identifier
        for c_go in list_go_interest[1:] : 
            filter_sparql_go += f" || (regex(?go, go:{c_go[-7:]}))"


        sparql.setQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
            PREFIX up: <http://purl.uniprot.org/core/>
            PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
            PREFIX go: <http://purl.obolibrary.org/obo/GO_>

            SELECT DISTINCT ?go_label

            WHERE {    
                VALUES ?gene_label {'""" + Gene + """'}  

                ?gene a up:Gene .
                ?gene skos:prefLabel ?gene_label .

                ?TF up:encodedBy ?gene .
                ?TF a up:Protein . 
                ?TF up:organism taxon:9606 .

                ?TF up:classifiedWith/(rdfs:subClassOf)* ?go .  
                ?go rdf:type owl:Class .
                ?go rdfs:label ?go_label . 

                FILTER (""" + filter_sparql_go + """) .
                
            }

        """)


        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        go_found = [result["go_label"]["value"] for result in results["results"]["bindings"]]

        for c_go in list_go_interest : 
            if c_go.split("_")[1][:-8] in go_found :  # to have only go term's label 
                col_annot[c_go[:-8]] = "X"  # TF count having this go term + its label (separated by '_')
            else :
                col_annot[c_go[:-8]] = "_"


    if list_categories_keywords : 
        ## Searchs for keywords that annotates TF :

        # Writes in sparql keywords' categories to select and unwanted keywords :
        filter_sparql_cat = f"(regex(?category_label, '{list_categories_keywords[0]}'))"
        for c_category in list_categories_keywords[1:] : 
            filter_sparql_cat += f" || (regex(?category_label, '{c_category}'))"


        if list_unwanted_keywords : 
            filter_sparql_key = f"(!regex(?keyword_label, '{list_unwanted_keywords[0]}'))" 
            for c_keyword in list_unwanted_keywords[1:] : 
                filter_sparql_key += f" && (!regex(?keyword_label, '{c_keyword}'))"
            
            filter_sparql_key = "FILTER (" + filter_sparql_key + ") ."

        else : 
            filter_sparql_key = ""

        sparql.setQuery("""
            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
            PREFIX owl: <http://www.w3.org/2002/07/owl#> 
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
            PREFIX up: <http://purl.uniprot.org/core/>
            PREFIX taxon: <http://purl.uniprot.org/taxonomy/>

            SELECT DISTINCT ?category_label (GROUP_CONCAT(DISTINCT ?keyword_label; separator = ", ") AS ?keywords)
            WHERE {

                VALUES ?gene_label {'""" + Gene + """'}  

                ?gene a up:Gene .
                ?gene skos:prefLabel ?gene_label .

                ?TF up:encodedBy ?gene .
                ?TF a up:Protein . 
                ?TF up:organism taxon:9606 . # Homo Sapiens
                ?TF up:classifiedWith ?keyword .

                ?keyword rdf:type <http://purl.uniprot.org/core/Concept> .
                ?keyword up:category/skos:prefLabel ?category_label .
                ?keyword skos:prefLabel ?keyword_label . 

                FILTER (""" + filter_sparql_cat + """) .
                """ + filter_sparql_key + """
            }

            GROUP BY ?category_label
        """)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()
        
        categories = [result["category_label"]["value"] for result in results["results"]["bindings"]]
        keywords = [result["keywords"]["value"] for result in results["results"]["bindings"]]

        for c_category in list_categories_keywords : 
            if c_category in categories :
                col_annot[c_category] = keywords[categories.index(c_category)]
            else : 
                col_annot[c_category] = ""

    return col_annot

def get_annot_tf(email:str, path_TF_table_annotated:str, follow_process_path:str, list_TF:str, sep_table:str, list_go_interest:str, list_unwanted_keywords:str, list_categories_keywords:[str], already_annotation_table, dict_annot_columns_id:str, max_MeSH_terms:str, min_MeSH_terms:str, get_G_citations_nb:bool) :
    '''

    '''

    t0 = time.time()

    # Checks existence of input files : 
    if not os.path.exists(list_TF):
        raise ValueError(f"{list_TF} does not exist. Change input.")

    # Checks if arguments are missing : 
    if not list_TF : 
        raise ValueError("Input file missing. (--list_TF)")

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

    if list_go_interest :
        if list_go_interest.count(":") == 0 : 
            raise ValueError(f"list_go_interst must be written as ', ' between each go terms an ':' between a label and its identifier like : 'transcription regulator activity:0140110, immune system process:0002376'")
        if not all([((type(go.split(':')[0]) == str) & (go.split(':')[1].isdigit()) & (len(go.split(':')[1]) == 7)) for go in list_go_interest.split(", ")]) :
            raise ValueError(f"list_go_interst must be written as ', ' between each go terms an ':' between a label and its identifier (that must have a length of 7 digit).\nExample : 'transcription regulator activity:0140110, immune system process:0002376'")

    if already_annotation_table :
        if not os.path.exists(already_annotation_table):
            raise ValueError(f"{already_annotation_table} does not exist. Change input.")
        if not dict_annot_columns_id :
            raise ValueError(f"If reuse a annotated TF table, must specify annotation columns number. ")

    # markdown_file to follow process :
    text_info = (f"""

# <font color=orange>Module get_annot_tf.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| path_TF_table_annotated | {path_TF_table_annotated} |
| follow_process_path | {follow_process_path} |
| list_TF | {list_TF} |
| sep_table | {sep_table} |
| list_go_interest | {list_go_interest} |
| list_unwanted_keywords | {list_unwanted_keywords} |
| list_categories_keywords | {list_categories_keywords} |
| already_annotation_table | {already_annotation_table} |
| dict_annot_columns_id | {dict_annot_columns_id} |
| max_MeSH_terms | {max_MeSH_terms} |
| min_MeSH_terms | {min_MeSH_terms} |
| get_G_citations_nb | {get_G_citations_nb} |


""")
    print(f"""

********************************************************************************
--> Module get_annot_tf.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(text_info)
    
    input_size = os.path.getsize(list_TF)

    list_all_TF = pd.read_csv(list_TF, sep = sep_table, header = 0, engine='python') 

    list_TF = list_all_TF[["Transcription_Factor"]].drop_duplicates()

    # Check if all TF annotation is already researched : 
    if already_annotation_table : 
        annot_table = pd.read_csv(already_annotation_table, sep = sep_table, header = 0, engine = 'python')
        list_TF = list_TF.merge(annot_table, how = "left")

        input_size += os.path.getsize(already_annotation_table)

        if type(dict_annot_columns_id) == str : 
            dict_annot_columns_id = ast.literal_eval(dict_annot_columns_id)

        # Defines what are annotations of interest :
        if dict_annot_columns_id["columns_go_terms"] :
            list_go_interest = []
            for i_col in dict_annot_columns_id["columns_go_terms"] :
                list_go_interest.append(list(annot_table.columns)[int(i_col)])
        else : 
            list_go_interest = None

        if dict_annot_columns_id["columns_keywords_categories"] :
            list_categories_keywords = []
            for i_col in dict_annot_columns_id["columns_keywords_categories"] :
                list_categories_keywords.append(list(annot_table.columns)[int(i_col)])
        else : 
            list_categories_keywords = None

    else :  # Searchs for annotations of interest if it is not already done 

        if list_go_interest : 
            list_go_interest = list_go_interest.split(", ")  # without the count of TF

        if list_unwanted_keywords : 
            list_unwanted_keywords = list_unwanted_keywords.split(", ")

        if list_categories_keywords : 
            list_categories_keywords = list_categories_keywords.split(", ")


    # Stores process and statistical informations in a txt file. 
    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    # with open(follow_process_path, "a") as info_file:
    #     # Informations on the srcipt :
    #     info_file.write("# Descriptions on query :\n") 
    #     info_file.write(f"- Script launched : **'get_annot_tf.py'**\n")
    #     info_file.write("- Script's description : provides new columns of annotation on transcription factors.\n")
    #     info_file.write(f"- Launched at **{datetime.datetime.today()}**\n") 
        
    #     info_file.write("## Annotations are :\n")
    #     info_file.write("- number of transcription factor paper's citation\n")
    #     info_file.write("- if go terms of interest annote TFs (X:yes, _:no)\t--> go terms selected:\n")
    #     if not already_annotation_table : 
    #         for c_go in list_go_interest :
    #             info_file.write(f"  - {c_go}\n")
    #     info_file.write("- TFs' uniprot's keywords with categories :\n")
    #     if not already_annotation_table : 
    #         for c_category in list_categories_keywords : 
    #             info_file.write(f"{c_category}\n\n")
    #     else : 
    #         info_file.write("\n")

    #     # informations on arguments :
    #     info_file.write(f"| Arguments used | |\n")
    #     info_file.write(f"| --- | --- |\n")
    #     info_file.write(f"| list_TF | {list_TF} |\n")
    #     if sep_table : 
    #         info_file.write(f"| sep_table | {sep_table} |\n")
    #     if list_go_interest : 
    #         info_file.write(f"| list_go_interest | {list_go_interest} |\n")
    #     if list_unwanted_keywords : 
    #         info_file.write(f"| list_unwanted_keywords | {list_unwanted_keywords} |\n")
    #     if list_categories_keywords : 
    #         info_file.write(f"| list_categories_keywords | {list_categories_keywords} |\n")
    #     if already_annotation_table : 
    #         info_file.write(f"| annot done | Yes |\n")


    if not already_annotation_table : 
        # To have the transcription factor total number annotated by a go term of interest : 

        if list_go_interest : 
            new_go_col_name = []
            for c_go in list_go_interest :
                sparql = SPARQLWrapper("https://sparql.uniprot.org/sparql/")

                sparql.setQuery("""
                    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
                    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
                    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
                    PREFIX skos: <http://www.w3.org/2004/02/skos/core#> 
                    PREFIX up: <http://purl.uniprot.org/core/>
                    PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
                    PREFIX go: <http://purl.obolibrary.org/obo/GO_>

                    SELECT DISTINCT (COUNT(DISTINCT ?gene_label) AS ?TF_nb)

                    WHERE {    
            
                        VALUES ?go { go:""" + c_go[-7:] + """ } .
                        VALUES ?go_TF { go:0140110 } . # Transcription regulator activity 

                        ?gene a up:Gene .
                        ?gene skos:prefLabel ?gene_label .

                        ?TF up:encodedBy ?gene .
                        ?TF a up:Protein . 
                        ?TF up:organism taxon:9606 .

                        ?TF up:classifiedWith/(rdfs:subClassOf) ?go .  
                        ?go rdf:type owl:Class .
                        
                        ?TF up:classifiedWith/(rdfs:subClassOf) ?go_TF .  
                        ?go_TF rdf:type owl:Class .
                    }

                """)

                sparql.setReturnFormat(JSON)
                results = sparql.query().convert()

                TF_nb = [result["TF_nb"]["value"] for result in results["results"]["bindings"]][0]
                new_go_col_name.append((str(TF_nb) + "_" + c_go))

                # adds TF_nb at the beginning of the go terms' label 

                list_go_interest = new_go_col_name 

        print(f"\nStarts to search transcription factors annotations : at {datetime.datetime.today()}.")

        # Perhaps rather than row[0], specifies with the columns label "Transcription_Factor"
        df_go = list_TF.apply(lambda row: get_TF_go_annot(email, row["Transcription_Factor"], list_go_interest, list_categories_keywords, list_unwanted_keywords, {}, min_MeSH_terms, max_MeSH_terms, get_G_citations_nb), axis = 1, result_type = "expand")

        # Calculates interesting citation percentage : adds columns next to columns of citations number
        if max_MeSH_terms : 
            df_go.insert((list(df_go.columns).index("nb_max_citations") + 1), "max_cit_per", round(((df_go["nb_max_citations"].astype(int) / df_go["nb_G_citations"].astype(int)) * 100), 1))
        if min_MeSH_terms : 
            df_go.insert((list(df_go.columns).index("nb_min_citations") + 1), "min_cit_per", round(((df_go["nb_min_citations"].astype(int) / df_go["nb_G_citations"].astype(int)) * 100), 1)) 

        list_TF = list_TF.merge(df_go, right_index = True, left_index = True)

        print(f"Starts to calculte statistics on data at {datetime.datetime.today()}.")

        # # prints information on annotations selected : 
        print("\nKeywords' category selected are : ")

        dict_annot_columns_id["columns_keywords_categories"] = []
        if list_categories_keywords :
            for c_category in list_categories_keywords : 
                print(f"- {c_category}")

                # stores columns index of columns labels : 
                dict_annot_columns_id["columns_keywords_categories"].append(list(list_TF.columns).index(c_category))

        dict_annot_columns_id["columns_go_terms"] = []
        if list_go_interest :
            print("\nGO terms of interest are (with its labels) : ")
            for c_go in list_go_interest : 
                print(f"- {c_go}")

                # stores columns index of columns labels : 
                dict_annot_columns_id["columns_go_terms"].append(list(list_TF.columns).index(c_go[:-8]))

        # print("\nUnwanted keywords are : ")
        # for c_unwanted_keyword in list_unwanted_keywords : 
        #     print(f"- {c_unwanted_keyword}")


        with open(follow_process_path, "a") as info_file:
            # Informations on the srcipt :
            info_file.write("\n# Execution's informations (time, memory):\n")
            info_file.write(f"\nSearching if transcription factors are annotated by go terms of interest taken **{round(((time).time() - t0), 3)} seconds <=> {round((((time).time() - t0)/60), 3)} minutes**.\n")

    list_all_TF = list_all_TF.merge(list_TF, how = "left")
    list_TF = []

    # Stores process informations : 

    # statistique informations on data  :
    text_info = """

 # Data analysis and statistics :
 
 
 ## Statistics on numerics TF table datas :

"""
    list_all_TF.to_csv(path_TF_table_annotated, sep = sep_table, index = None)

    if max_MeSH_terms : 
        # makes 7 intervals (to not have to much lines) of citations counts, but tu gather TF as a list with their nb : 


        pipeline.stat(list_all_TF[["nb_max_citations"]], "nb_max_citations", "hist, box", "\t", f"{follow_process_dir}/graphics")

        text_info += f"""


## nb_max_citations : 
***

{pipeline.display_table_stat("graphics/nb_max_citations_hist.png", "nb_max_citations_hist", "graphics/nb_max_citations_box.png" , "nb_max_citations_box", pipeline.makes_intervals(list_all_TF[["Transcription_Factor", "nb_max_citations"]], "nb_max_citations", 7).to_markdown())}

***
"""
# {list_all_TF[["nb_max_citations"]].astype(float).describe().to_markdown()}

# ***

# """
        
    if min_MeSH_terms : 
        pipeline.stat(list_all_TF[["nb_min_citations"]], "nb_min_citations", "hist, box", "\t", f"{follow_process_dir}/graphics")

        text_info += f"""


## nb_min_citations : 
***

{pipeline.display_table_stat("graphics/nb_min_citations_hist.png", "nb_min_citations_hist", "graphics/nb_min_citations_box.png" , "nb_min_citations_box", pipeline.makes_intervals(list_all_TF[["Transcription_Factor", "nb_min_citations"]], "nb_min_citations", 7).to_markdown())}

***
"""
# {list_all_TF[["nb_min_citations"]].astype(float).describe().to_markdown()}

# ***
# """

    if get_G_citations_nb : 
        pipeline.stat(list_all_TF[["nb_G_citations"]], "nb_G_citations", "hist, box", "\t", f"{follow_process_dir}/graphics")

        text_info += f"""

## nb_G_citations : 
***

{pipeline.display_table_stat("graphics/nb_G_citations_hist.png", "nb_G_citations_hist", "graphics/nb_G_citations_box.png" , "nb_G_citations_box", pipeline.makes_intervals(list_all_TF[["Transcription_Factor", "nb_G_citations"]], "nb_G_citations", 7).to_markdown())}

***
"""
# {list_all_TF[["nb_G_citations"]].astype(float).describe().to_markdown()}

# ***
# """

    # Calculates how many TF's are annotated by go terms of interest : 
    if list_go_interest :
        df_count_TF_go = {}
        for go_interest in list_go_interest :
            if not already_annotation_table : 
                df_count_TF_go[go_interest[:-8]] = [list(list_all_TF[go_interest[:-8]]).count("X"), ", ".join(list_all_TF[list_all_TF[go_interest[:-8]] == "X"]["Transcription_Factor"])]
            else :
                df_count_TF_go[go_interest] = [list(list_all_TF[go_interest]).count("X"), ", ".join(list_all_TF[list_all_TF[go_interest] == "X"]["Transcription_Factor"])]

        go_terms_count_stat = pd.DataFrame({"go_terms" : list(df_count_TF_go.keys()), "count_per_go_terms" : [value[0] for value in list(df_count_TF_go.values())], "TF" : [value[1] for value in list(df_count_TF_go.values())]})

        go_terms_count_stat.plot(kind = "bar", x = "go_terms", y = "count_per_go_terms", figsize = (10,12), legend = True, title = "count_per_go_terms")
        plt.savefig(f"{follow_process_dir}/graphics/count_per_go_terms_bar.png")
        plt.close()

        text_info += f"""

## Table of number of TF with annotation and their labels :
***

<table>
    <thead>
        <tr>
            <th>graphic</th>
            <th>table</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4><img src=graphics/count_per_go_terms_bar.png alt=graĥic width="500"/></td>
            <td rowspan=2>

{go_terms_count_stat.to_markdown()}

</td>
        </tr>
        <tr>
        </tr>
    </tbody>
</table>

***
"""

    # Counts how many TF are annotated by a given keyworks and which ones : 
    if list_categories_keywords :
        for c_category in list_categories_keywords : 
            category_table = {}
            for keywords, TF in zip(list_all_TF[c_category], list_all_TF["Transcription_Factor"]) :
                if str(keywords) == "nan" :
                    continue
                keywords = keywords.split(", ")
                for c_keyword in keywords : 
                    if c_keyword in category_table.keys() :
                        category_table[c_keyword][0] += 1
                        category_table[c_keyword][1] += ", " + TF
                    else : 
                        category_table[c_keyword] = [1, TF]

            category_table = pd.DataFrame({"keywords" : list(category_table.keys()), "keywords_count" : [value[0] for value in list(category_table.values())], "TF" : [value[1] for value in list(category_table.values())]})

            pipeline.stat(category_table[["keywords", "keywords_count"]], "keywords_count", "hist, box", "\t", f"{follow_process_dir}/graphics")

            category_table.plot(kind = "bar", x = "keywords", y = "keywords_count", figsize = (10,12), legend = True, title = "keywords_count")
            plt.savefig(f"{follow_process_dir}/graphics/keywords_count_bar.png")
            plt.close()

            text_info += f"""

## Keywords count on category {c_category} :
***

<table>
    <thead>
        <tr>
            <th>graphic</th>
            <th>table</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4><img src=graphics/keywords_count_bar.png alt=graĥic width="500"/></td>
            <td rowspan=2>

{category_table.to_markdown()}

</td>
        </tr>
        <tr>
        </tr>
    </tbody>
</table>

***
"""


    print("\nFirst lines of the TF's annotated table are : ")
    print(list_all_TF.head())

    # Cheks if directory exists 
    if not os.path.exists("/".join(path_TF_table_annotated.split("/")[:-1])):
        os.makedirs("/".join(path_TF_table_annotated.split("/")[:-1]))

    list_all_TF.to_csv(path_TF_table_annotated, sep = sep_table, index = None)

    if already_annotation_table : 
        path_annotated_table = already_annotation_table  # Path to all TF annotated table 
    else : 
        # stores columns index for columns index labels in a dictionnary : 
        if get_G_citations_nb : 
            dict_annot_columns_id["column_G_citations"] = list(list_all_TF.columns).index("nb_G_citations")
        else : 
            dict_annot_columns_id["column_G_citations"] = None
        if max_MeSH_terms :    
            dict_annot_columns_id["column_max_MeSH_citations"] = list(list_all_TF.columns).index("nb_max_citations")
        else : 
            dict_annot_columns_id["column_max_MeSH_citations"] = None
        if min_MeSH_terms : 
            dict_annot_columns_id["column_min_MeSH_citations"] = list(list_all_TF.columns).index("nb_min_citations")
        else : 
            dict_annot_columns_id["column_min_MeSH_citations"] = None 
        path_annotated_table = path_TF_table_annotated

    text_info += f"""

***

### dict_annot_columns_id : (if the annotation table will be reused in an other query)


{dict_annot_columns_id}


### Columns labels :

{list_TF.columns}

""" 

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
        follow_process_file.write(f"\n\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes\n")

    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [dict_annot_columns_id, path_annotated_table, [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]]


if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-tf", "--list_TF", help = "Path to the transcription factor selected by Regulus list.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in transcription table of interest.")
    parser.add_argument("--email", help = "email address to informs who launch query to nbci (mandatory).")
    parser.add_argument("-go", "--list_go_interest", help = "String of go terms of interest separated by ','. Example : 'cellular developmental process:0048869,immune system process:0002376,B cell activation:0042113,plasma cell differenciation:0002317'")
    parser.add_argument("-kw", "--list_unwanted_keywords", help = "Specific keywords that are not wanted.")
    parser.add_argument("-ckw", "--list_categories_keywords", help = "Wanted categories' keywords (as : Molecular funciton, Biological Process...) ")
    parser.add_argument("-aan", "--already_annotation_table", help = "Path to the all transcription factor annotated table.")
    parser.add_argument("--dict_annot_columns_id", help = "Dictionnary of annotations columns number given by an other query (in the follow process file) to reuse annotations of a table.")
    parser.add_argument("-max_cit", "--max_MeSH_terms", help = "Maximum list of MeSH terms (labels) that papers must be annotated with to be counted.")
    parser.add_argument("-min_cit", "--min_MeSH_terms", help = "Maximum list of MeSH terms (labels) that papers must be annotated with to be counted.")
    parser.add_argument("-cit", "--get_G_citations_nb", help = "  Bool, if general papers' citation for this TF number must be return as a column.", action = "store_true", default = True)
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("--path_TF_table_annotated", help = "Stores file's name's transcription factors annotated.")

    # add one for the SPARQL endoint ?

    args = parser.parse_args()

    get_annot_tf(args.email, args.path_TF_table_annotated, args.follow_process_path, args.list_TF, args.sep_table, args.list_go_interest, args.list_unwanted_keywords, args.list_categories_keywords, args.annotation_table, args.dict_annot_columns_id, args.max_MeSH_terms, args.min_MeSH_terms, args.get_G_citations_nb)