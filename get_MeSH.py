#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description :

This script provides MeSH terms count from a paper set of interest.
In order to select MeSH terms of interest to get annotations of transcription factors.
This list of transcription factors can be one resturns by the pipeline or a list of known TFs.
This set can also be all apers annotating transcription factors selected. 

Documentation for biopython : http://biopython.org/DIST/docs/tutorial/Tutorial.html#sec158

User provides a set of pmid of interest. Search direct on pubmed, possible to have similar articles...

example : 
/home/eveadmin/Bureau/Stage_Eve_2021/pmid_list_mesh_pmid.txt

--> Arguments : 

    "-pmid", "--pmid_interest_file_path" : Path to the file containing the list of pmid of interest. (in column)
    "-o", "--output_mesh_file" : Path to the file containing the table of mesh count.
    "-tft", "--Transcription_Factors_Table" : Path to the file containing the table of transcription factors.
    "-max", "--max_pmid_per_TF_nb" : Maximum number of papers annotating a transcription factor.
    "-uri", "--is_uri" : Bool, True if list of pmid given is a uri containing pmid list.", action = "store_true

--> Proceeding : 

    - if a trancriptionf factors' table is given (-tft) :
        - searchs pmid of papers cited each TFs
        - until a maximum citation number given (-max) 
    - if a pmid list is given (-pmid)
        - finds the pmid in the url text if it is a list of url (-is_url)
    - searchs and gathers all MeSH terms associated to pmids
    - counts pmid 
    - counts distinct MeSH terms 
    - stores the count in a table (-o)

--> A command line can be : 
python3 get_MeSH.py -pmid "/home/eveadmin/Bureau/Stage_Eve_2021/pmid_list_mesh.txt" -o "/home/eveadmin/Bureau/Stage_Eve_2021/mesh_interest_2.csv -uri"
python3 get_MeSH.py -tft TF_tables/TF_table_q95.csv -o "/home/eveadmin/Bureau/Stage_Eve_2021/mesh_interest_q95.csv" -max 10
python3 get_MeSH.py -tft TF_tables/TF_connus.txt -o "/home/eveadmin/Bureau/Stage_Eve_2021/mesh_interest_connus.csv" -max 10

'''

# Importations :
from Bio import Entrez, Medline
import pandas as pd
import argparse 
import time 
import datetime 

import matplotlib.pyplot as plt
import os
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage

def add_MeSH(pmid:int, MeSHs_list:[]) -> [str] :
    '''
    Adds MeSH terms annoting paper with pmid given in a given list.
    It returnes the list of MeSh updated.
    '''
    # Search MeSH term associated with id paper having pmid 
    handle = Entrez.efetch(db = "pubmed", id = int(pmid), rettype = "medline", retmode = "text")
    records = Medline.parse(handle)
    for record in records:
        if record.get("MH") != None :
            MeSHs_list.extend([MeSH.split("/")[0].replace("*", "") for MeSH in record.get("MH")])  # In order to have a global count

    return MeSHs_list

def get_MeSH(email, follow_process_path, pmid_interest_file_path, output_mesh_file, Transcription_Factors_Table, max_pmid_per_TF_nb, is_uri, sep_table) :
    '''
    '''

    t0 = time.time()

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module get_MeSH.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| pmid_interest_file_path | {pmid_interest_file_path} |
| follow_process_path | {follow_process_path} |
| Transcription_Factors_Table | {Transcription_Factors_Table} |
| max_pmid_per_TF_nb | {max_pmid_per_TF_nb} |
| sep_table | {sep_table} |
| is_uri | {is_uri} |

""")
    print(f"""

********************************************************************************
--> Module get_MeSH.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(text_info)

    Entrez.email = args.email  # "eve.barre.1@etudiant.univ_rennes1.fr" # register your email

    if (Transcription_Factors_Table != None) & (pmid_interest_file_path != None) : 
        raise ValueError("a transcription factor list or a pmid table must be given, not both.")

    # from a transcription factors' list given : 
    if Transcription_Factors_Table : 

        # Checks existence of input files : 
        if not os.path.exists(Transcription_Factors_Table):
            raise ValueError(f"{Transcription_Factors_Table} does not exist. Change input.")

        input_size = os.path.getsize(Transcription_Factors_Table)

        # 1) Select paper's pmid annoting transcription factors of interest : 
        pmid_list = []
        TF_table = pd.read_csv(Transcription_Factors_Table, header = 0, sep = sep_table, usecols = ["Transcription_Factor"])

        if max_pmid_per_TF_nb : 
            max_cit = max_pmid_per_TF_nb    
        else : 
            max_cit = None

        for TF in TF_table["Transcription_Factor"] : 
            handle = Entrez.esearch(db = "pubmed", term = f"{TF}", retmax = max_cit)
            record = Entrez.read(handle)
            handle.close()
            pmid_list.extend(record["IdList"])
        
        # Drops duplicates : 
        pmid_list = list(set(pmid_list))
        pmid_count = len(pmid_list)

        # 2) Gets all mesh terms associated with papers of interst : 
        MeSHs_list = []

        for pmid in pmid_list : 
            MeSHs_list = add_MeSH(pmid, MeSHs_list)


    # From a pmid's list given :
    elif pmid_interest_file_path : 

        # Checks existence of input files : 
        if not os.path.exists(pmid_interest_file_path):
            raise ValueError(f"{pmid_interest_file_path} does not exist. Change input.")

        input_size = os.path.getsize(pmid_interest_file_path)

        with open(pmid_interest_file_path, "r") as pmid_file : 

            pmid_count = 0
            MeSHs_list = []

            for pmid in pmid_file :  # Gets all mesh terms associated with papers of interst : 
                
                # Depends on how pmid are written : 
                if is_uri :
                    pmid = [int(id_paper) for id_paper in pmid.split("/") if id_paper.isdigit()][0]
        
                MeSHs_list = add_MeSH(pmid, MeSHs_list)
                # count papers nb :
                pmid_count += 1
    

    table_MeSH = pd.DataFrame({"MeSHs":MeSHs_list})
    table_MeSH = table_MeSH.groupby(["MeSHs"])["MeSHs"].count().reset_index(name = f"MeSHs_nb_on_{pmid_count}")
    table_MeSH = table_MeSH.sort_values(by = [f"MeSHs_nb_on_{pmid_count}"], ascending = False)

    if not os.path.exists("/".join(output_mesh_file.split("/")[:-1])):
        os.makedirs("/".join(output_mesh_file.split("/")[:-1]))
    
    a = str(sep_table)

    table_MeSH.to_csv(output_mesh_file, index = None, sep = a)

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]

    # Add generation of pmid from a pmid query 

if __name__ == "__main__" : 

    # Arguments : 
    parser = argparse.ArgumentParser()

    parser.add_argument("-pmid", "--pmid_interest_file_path", help = "Path to the file containing the list of pmid of interest. (in column)")
    parser.add_argument("-o", "--output_mesh_file", help = "Path to the file containing the table of mesh count.")
    parser.add_argument("-tft", "--Transcription_Factors_Table", help = "Path to the file containing the table of transcription factors.")
    parser.add_argument("-max", "--max_pmid_per_TF_nb", help = "Maximum number of papers annotating a transcription factor.")
    parser.add_argument("-uri", "--is_uri", help = "Bool, True if list of pmid given is a uri containing pmid list.", action = "store_true")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")
    parser.add_argument("--email", help = "email address to informs who launch query to nbci (mandatory).")

    args = parser.parse_args()

    get_MeSH(args.email, args.follow_process_path, args.pmid_interest_file_path, args.output_mesh_file, args.Transcription_Factors_Table, args.max_pmid_per_TF_nb, args.is_uri, args.sep_table)