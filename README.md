
# Tool developped during my intership. 


## <font color=red>Tool explanations :</font>


FC : Il faut changer le titre dans le lien du readme (too -> tool)

This tool helps the analysis of a RDF (Resource Description Framework) regulatory network of transcription factor - gene relations.

FC : pourquoi se limiter à une sortie d'un pipeline RDF ? Ton outil est applicable à toute table qui pourrait ressembler aux nôtres.
Par contre, ce que tu pourrais faire ici, c'est décrire précisément la table d'entrée, puisque c'est elle l'objet principal du traitement que tu vas faire.

There are several modules with diverses functions, described below. Their usage is flexible and they are grouped in tree parts:

1. The first part is **data preprocessing and query preparation**.

- The module ***relations_table_analysis_1*** provides basic statistics about input data. For example it indicates how many genes and transcription factors (TF) compose the dataset, the ditribution of genes count per expression pattern, etc... These information will help to better choose parameters for other modules (see group_pattern in part 2). 
- The module ***get_targets_regulators_1*** is independant to others and describes each gene interactions with other genes in the network. The output is a table of each gene regulators (transcription factors that target it positively or negatively) and targets (if the gene is a transcription factor). If you are particularly interested in a candidate gene, you can have a first glance of its relations here.
- ***graph_go*** helps the user to choose specific Gene Ontology (GO) terms by showing the hierarchical graph of a given GO term (for example, one may want to be more or less specific by choosing an descendant or a parent of the starting GO term). This will help for the use of the module ***get_annot_tf*** query which provides annotations of transcription factors according to the user-defined GO terms of interest. 
- ***get_annot_tf*** can retrieve how many articles (FC : où ça, dans quelle base de données) cite a TF either without any restriction or when an article is annotated by a MeSH term of interest. The choice of Mesh terms used for these queries is guided by the module ***get_MeSH***. From a set of articles (allows the user to use the litterature about the sunject of interest) or of TFs, ***get_MeSH*** produces a table of MeSH terms found in this set and the count of articles per term. If a MeSH term is more represented, it can be one that well represent the biological context.
In ***get_annot_tf*** two queries can be given : a more general one condition, using only one or few terms and a more stringent condition, using a very precise combination of terms. These terms can be combined by the usage of the logical operators OR and AND.

FC : donner les noms des paramètres correspondant aux requêtes strigente ou non (éventuellement changer, car min_... et max_... ne me semblaient pas forcément idéales). N'oublie pas de mettre des exemples aussi.

- Finally, ***get_annot_tf_1*** can be launched directly from an initial set of transcription factor before their selection (part 2) if several queries will be done not to research several times same information. FC : ce n'est pas très clair

FC : la séparation de graph_go, get_mesh t get_annot_tf n'est pas claire. Soit tu présentes get_annot_tf en premier et tu dis qu'il dépend des deux autres que tu décris après, soit tu décris les deux premiers indépendamment, mias ça me paraît plus compliqué.

2. The second part is **major transcription factors selection**.
- The selection is done on TF's coverage and specificity for a gene pattern. Coverage depict which partof the gene pattern is regulated by a specific TF. Specificity represents the number of targets of a TF which belong to a specific pattern i.e., if a TF preferentially regulates one gene expression pattern over the others. 
Both coverage and specificity are calculated as percentages and computed with the genes count per gene pattern. Some patterns regroup only few genes and may bias the coverage and specificity distributions. Therefore, ***A_group_pattern*** proposes to delete gene patterns having small counts and/or gather them with their opposed patterns, on the hypothesis that opposed patterns are regulated oppositely.
- ***B_select_tf*** produces specificity and coverage calculations. Once computed, percentages are used to compare TF and select major relations. A threshold can be set as mean + one standard deviation, a quantile (FC : quantile ou quartile ?) and/or a specific percentage. 
- TF's annotations can be provided, on the selected TF, with ***C_get_annot_tf***. Another possibility for TF annotation is to specify a keyword of diverse possible categories to have a global view of TF associated context. (FC : je n'ai pas compris la ernière phrase) 

3. The last part is the **analysis of results and displays**.
- After data modifications, it can be useful to launch ***relations_table_analysis_3*** again. (FC : dans quel but ?)
- In the same way, ***get_targets_regulators_3*** can also be launched with the selected TF and genes (FC : patterns ?). 
- Several graphs can be provided to picture TF's selected interactions. ***get_gene_pattern_per_TF_graph*** produces a graph of how gene patterns regulated by a given TF are in relation (are they opposed, quite opposed, similar or quite similar). It helps to see global regulation by a TF of interest.
- It can be the same but per TF's pattern with ***get_gene_pattern_per_TF_pattern_graph*** with even more general (FC : general what ?). FC : je n'ai pas compris ce que fait cet outil ? Quelle information est donnée ?
- To have a whole view : ***get_TF_pattern_graph*** shows by a graph how TF's pattern selected are in relation. the goal here is to see if groups are formed and if patterns's regulations could be in the same way.
FC : est-ce la relation des profils de TF entre eux que tu veux montrer ? Dans ce cas, c'est une régulation de certains profils par d'autres ? C'est intéressant et ça permet de voir une dynamique dans le graph, mais je n'avais pas compris que tu avais fait ça aussi...
- Because query's possibilities are numerous, ***compares_TF*** allows to compares TF provided by two different manners. It precises common and specific TF from the two tables supplied. This new information can support final parameters choice.
FC : ça compare quoi exactement ? Le résultat de deux sélection couverture / spécificité ? On est d'accord que les requêtes d'annotation ne viennent filtrer qu'après ?
- In order to follow TF selection, ***update_class*** (launched between each filtering module) classifies TF in a table where each line is a filter. It enables to check through which filter a TF of interest is lost for instance.

FC : c'est pas mal, mais je pense, comme disait Marine qu'un graph qui montre l'enchaînement et les relations entre les modules est plus que nécessaire.


| Parts | explanations | modules' name |
| --- | --- | --- |
| 1. Data preprocessing and query preparation | Data statistics | relations_table_analysis_1 |
|  |  |  |
|  |  |  |
|  |  |  |
| 2. Major transcription factors selection |
| 3. Resutls analysis and displays |


Find following modules' explanations :

| 1. Data preprocessing and query preparation | 2. Selection of major transcription factors | 3. Analyse of resutls and display |
| --- | --- | --- |
| relations_table_analysis | group_pattern | get_gene_pattern_per_TF_graph |
| graph_go | select_tf | get_gene_pattern_per_TF_pattern_graph |
||  | compares_TF |
| get_MeSH |  | update_class |
| get_annot_tf | get_annot_tf | get_TF_pattern_graph |
| get_targets_regulators |  | get_targets_regulators |

***

## Tests : 

relation_table_test : 0.005% random lines of initial Cell B data relation table. ()   


## <font color=red>prerequisite / Before to use :</font>

### Installations : 

- python 
- pandas
- matplotlibplot
- pygraphviz
- time
- datetime 
- os
- gooey
- from Bio import Entrez, Medline
- sudo apt-get install sox


...

Define the path to scripts as a pypath. The way to do depends on our shell.
**Or** defines a configuration file. 
Input tables must have headers and output tables will have.

***

### <font color=red>Modules description :</font> 


### How to use / choose modules : 


- get_rel_tf --> before or after selection of transcription factors of interest data 
- update_class --> between each filter
- get_MeSH --> before get_annot_tf

1) graph_go / graph_MeSH /
1) get_annot_tf (here if severals query will be launched otherwise, after select_tf)
    - update_class
2) group_pattern 
3) select_tf
    - update_class
4) get_annot -an
5) get_gene_pattern_per_TF_graph / get_gene_pattern_per_TF_pattern_graph
6) compares_TF


Filters : 
    - all TF
    - differential expression +/- constant
    - gene per gene pattern number (minimum threshold)
    - compatibility table 
    - specificity + coverage --> several threshold ?
    - ? : annotations, publication's citation number ... 


***

### <font color=orange>setup</font>

### <font color=orange>pipeline</font>


Scripts that lauched modules in function of arguments.


## 1) Data preprocessing and query preparation 


### <font color=orange>relations_table_analysis :</font>


This scripts provides general statistics (histogram and boxplot) on table data.


### <font color=orange>graph_go :</font> 


This script provides a hierargical graph of parents go terms of a go terms of interest. 
The goal here is to help the choice of go terms of interest to know if transcription factors 
selected are annotated with. 

example for plasma cell differenciation : GO:0002317
several are possible (séparates by coma)
B cell mediated immunity GO:0019724

A command line example : 
*python graph_go -go "0002317,0019724" -o graph_go.png*

### <font color=orange>graph_MeSH :</font> 


This script provides a hierargical graph of parents MeSH terms of a MeSH terms of interest. 
The goal here is to help the choice of MeSH terms to query with 
(for the count of transcription factor's citations). 

example for : 
- Cell Differentiation : D002454
- B-Lymphocytes : D001402

MeSH endpoint : https://id.nlm.nih.gov/mesh/sparql 
documentation : https://hhs.github.io/meshrdf/sparql-and-uri-requests 

requiers pygraphviz and networkx

matplotlib requiered version : 2.2.3

A command line example : 
python graph_MeSH -MeSH "B-Lymphocytes:D001402" -o graph_MeSH.png


### <font color=orange>get_MeSH :</font>


This script provides MeSH terms count from a pmid's paper set of interest (given or annotating transcription factors of interest). In order to select MeSH terms of interest to get annotations of transcription factors.

A command line to launch could be : 
python3 get_MeSH -pmid "/home/eveadmin/Bureau/Stage_Eve_2021/pmid_list_mesh_pmid.txt" -o "/home/eveadmin/Bureau/Stage_Eve_2021/mesh_interest.csv"


***


##  2) Selection of major transcription factors





### <font color=orange>select_tf :</font> 


This scripts selects relations between transcription factor (TF) and gene on the specificity and the coverage of TF for a gene's pattern. 

The command line should be like : 
python select_tf -i resultSain.csv -ttf TF_table_meanstd.csv -tcg nb_genes_by_pattern_B.txt -presel Rel_preselected.csv -th mean+std
python select_tf -i resultSain.csv -ttf TF_table_q75.csv -tcg nb_genes_by_pattern_B.txt -th q75
python select_tf -i preprocessed_relations.csv -ttf TF_tables\TF_table_q75_pre.csv -tcg gene_per_gene_pattern\gene_per_pattern_nb_B_pre.csv -th q75


### <font color=orange>get_annot_tf :</font> 


This scripts adds annotations' colomns of transcription factors (TF) :
-  number of paper's citation,
- if the transcription factor is annotated by go terms of interest (X:yes, _:no),
-  uniprot's keywords associated. 

A command example can be : 
python get_annot_tf -tf TF_tables\TF_table_meanstd.csv -s "\t"
python get_annot_tf -tf TF_tables\TF_table_meanstd.csv -s "\t" -an annotation_all_TF_ini.csv -col_cat "3,4" -col_go "5,6,7,8,9,10"

### <font color=orange>group_pattern :</font> 


Preprocess TF-Gene-pattern relations before selection on specicity and coverage by allowing theses future calculations groupped by strict opposed pattern. 
It can gather gene's pattern with a maximun gene's number as a given threshold. 
The scripts provides a new gene's per gene's pattern count table and a new relation's table.

Command line launched is like : 
python group_pattern -i resultSain.csv -gct gene_per_pattern_nb\gene_per_pattern_nb_B.csv -or preprocessed_relations.csv -max 100



***


## 3) Analyse of resutls and display


### <font color=orange>get_rel_tf :</font> 


This scripts provides two csv files (regulators +/-and targets) of relations concerning a gene of interest (That can be a transcription factor or not) among a csv file containing relations' informations.

The command line should be like :
"python get_rel_tf -i TF_Gene_allProfile_reg.csv -g TP63"

### <font color=orange>get_TF_pattern_graph :</font> 


This script provides the graph figure (.png) of TF' patterns selected.
Relation are about if tf's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).


### <font color=orange>get_gene_pattern_per_TF_graph :</font> 


Script get_pattern_graph_TF stores the graph figure (.png) of pattern's targeted by a given transcription factor.
Relation are about if gene's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).

- direct opposed edge = red
- undirect opposed edge = orange
- similar edge = green 
- positive regulation = "+"
- negative regulation = "-"

A command line to launch can be like : 
python get_pattern_graph_TF -i TF_tables\TF_table_q75.csv -tf PRDM1 

### <font color=orange>get_gene_pattern_per_TF_pattern_graph :</font> 


This script provides the graph figure (.png) of genes' pattern's targeted by a transcription factor's pattern given.
Relation are about if gene's patterns are similar or quite opposed (+/- 1 authorized for one digit of the pattern).
 (see get_gene_pattern_per_TF_graph)




### <font color=orange>compares_TF :</font> 


This scripts compares transcription factors' lists and returns TF that are specific and common to tables.

A command line to use can be : 
python compares_TF -t1 TF_tables\TF_table_q75_pre.csv -t2 TF_tables\TF_table_q75_pre_100.csv -n no_vs_100


### <font color=orange>update_class :</font>


This scripts updates the result table that class transcription factors in funciton of filter pass.

A command line to use can be : 
python update_class -t TF_tables\TF_table_q75.csv -n specificity_coverage -ct class_table.csv


# csv files of results : 

## Rel_cov_spe_preselected.csv :


Reltations TF-Gene with specificities and coverages before selection on it. 


## resultSain.csv : 


Relations of TF-Gene without specificities and coverage before selection. 
(Result of Regulus) with healthy B cell data. 

## Comp_TF_result.md : 


This file shows result of transcription factor comparison between two table.


Global prerequisite : 
pandas
numpy
os
time
datetime
resource
argparse

matplotlibplot


## <font color=red>Part 1 _ Data preprocessing and query preparation</font>


| Module's name | Description | Input | Prerequisite |
| --- | --- | --- | --- |
| relations_table_analysis_1 | general statistics on data | relation_table_path, genes_per_gene_pattern_count_table_path ||
| graph_go | hierarchical graph of go terms | go_terms, output_file ||
| get_MeSH | papers' MeSH count | pmid table and if is uri or TF table and max pmid per TF | Bio (Entrez, Medline) |
| get_annot_tf_1 | TF annotations (go, citations number, keywords) | TF list, path_TF_table_annotated, list_go_interest, list_unwanted_leywords, list_categories_keywords, max_MeSH_terms, min_MeSH_terms, get_G_citations_nb | Bio (Entrez), SPARQLWrapper |
| get_targets_regulators_1| Genes targets and regulators | relations_table, Genes, result_path ||

<!-- | get_rel_tf_1 | selects specific genes targets and regulators | relations_table, Gene, result_path || -->

***

## <font color=red>Part 2 _ Selection of major transcription factors</font>



| Module's name | Description | Input | Prerequisite |
| --- | --- | --- | --- |
| A_group_pattern | gathers (or deletes) opposed gene's patterns | min and max digit (in patterns), genes_per_gene_pattern_count_table, relations_table, max_genes_per_pattern_nb, gene_patterns_to_delete, delete_small_gene_patterns ||
| B_select_tf | select TF on their coverage and specificity | table_tf, threshold_type, percentage_threshold_cov_spe, columns_TF, genes_patterns_to_delete, preselected_table, table_count_gene_by_pattern ||
| C_get_annot_tf | TF annotations (go, citations number, keywords) | TF list, path_TF_table_annotated, list_go_interest, list_unwanted_keywords, list_categories_keywords, max_MeSH_terms, min_MeSH_terms, get_G_citations_nb | Bio (Entrez), SPARQLWrapper |

***

## <font color=red>Part 3 _ Analyse of resutls and display</font>



| Module's name | Description | Input | Prerequisite |
| --- | --- | --- | --- |
| get_targets_regulators_3 | Genes targets and regulators | relations_table, Genes, result_path ||
| get_TF_pattern_graph | TF patterns' interaction's graph (is similar or opposed) | max and min digit (in patterns), max distance, relation_table | networkx |
| get_gene_pattern_per_TF_graph | gene's patterns interaction per TF's graph | max and min digit (in patterns), max distance, relation_table, TF | networkx |
| get_gene_pattern_per_TF_pattern_graph | gene's patterns interaction per TF's pattern's graph | max and min digit (in patterns), max distance, relation_table, TF patterns | networkx |
| compares_TF | compares two TF tables  | TF_table_1, TF_table_2, comparison_name, output_file ||
| update_class | TF classification depends on filters | filter_name, TF_table, dict_annot_columns_id ||
| relations_table_analysis_3 | general statistics on data | relation_table_path, genes_per_gene_pattern_count_table_path ||

<!-- | get_rel_tf_3 | Specific genes targets and regulators | relations_table, Gene, result_path || -->
