#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

This scripts provides general statistics (histogram and boxplot) on table data.

--> Arguments :

    "--relation_table_path", help = "Path to the file containing relations' information.")
    "-gct", "--genes_per_gene_pattern_count_table", help = "Path to the file containing the count of genes per gene's pattern.")
    "--follow_process_path", help = "Path to the file to save process data.")
    -s", "--sep_table", help = "String that separates columns in tables.")


--> Proceeding : 

--> A command line example to launch :

'''

import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import time 
import datetime
import argparse
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage

import pipeline


def relations_table_analysis(relation_table_path:str, genes_per_gene_pattern_count_table_path:str, sep_table:str, follow_process_path:str) :
    '''
    '''

    t0 = time.time()

    # cheks input path existance : 
    if not os.path.exists(relation_table_path):
        raise ValueError(f"{relation_table_path} does not exist. Change input.")

    if not os.path.exists(genes_per_gene_pattern_count_table_path):
        raise ValueError(f"{genes_per_gene_pattern_count_table_path} does not exist. Change input.")

    input_size = os.path.getsize(relation_table_path) + os.path.getsize(genes_per_gene_pattern_count_table_path)

    # input data import : 
    relations_table = pd.read_csv(relation_table_path, sep = sep_table, header = 0)
    gene_per_gene_pattern_count_table = pd.read_csv(genes_per_gene_pattern_count_table_path, sep = sep_table, header = 0)

    line_nb_no_drop = relations_table.shape[0]
    relations_table = relations_table.drop_duplicates()
    line_nb = relations_table.shape[0]

    follow_process_dir = "/".join(follow_process_path.split("/")[:-1])

    if not os.path.exists(follow_process_dir):
        os.makedirs(follow_process_dir)
    
    print(f"""

********************************************************************************
--> Module relation_table_analysis.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    text_info = f"""

# <font color=orange>Module relation_table_analysis.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| relation_table_path | {relation_table_path}  |
| genes_per_gene_pattern_count_table_path | {genes_per_gene_pattern_count_table_path}  |
| sep_table | {sep_table} |
| follow_process_path | {follow_process_path} |


# Relations' table analysis : 


- Drop_duplicates : **{line_nb_no_drop != line_nb}**
- Relation_table lines number : **{line_nb}**
- Relation_table columns number : **{relations_table.shape[1]}**
- Columns labels : **({[column for column in relations_table.columns]})**


## Distinct values count table :


"""

    table_stat = pd.DataFrame({column:[(relations_table[[column]].drop_duplicates().shape[0])] for column in relations_table.columns})

    with open(follow_process_path, "a") as follow_process_file : 
        follow_process_file.write(text_info)
        table_stat.to_markdown(follow_process_file)

    text_info = """


## Regulatory count : 


"""
    table_stat = pd.DataFrame({"Reg -" : [relations_table[relations_table["Reg"] == "-"].shape[0], (str(round((((relations_table[relations_table["Reg"] == "-"].shape[0]) / line_nb) * 100), 1)) + " %")], "Reg +" : [relations_table[relations_table["Reg"] == "+"].shape[0], (str(round((((relations_table[relations_table["Reg"] == "+"].shape[0]) / line_nb) * 100), 1)) + " %")]})

    with open(follow_process_path, "a") as follow_process_file : 
        follow_process_file.write(text_info)
        table_stat.to_markdown(follow_process_file)

    table_stat_1 = relations_table[["Transcription_Factor", "Gene"]].groupby(["Transcription_Factor"]).Gene.count().rename("Genes_per_TF_count").to_frame().reset_index()  
    pipeline.stat(table_stat_1, "Genes_per_TF_count", "hist, box", "\t", f"{follow_process_dir}/graphics")

    table_stat_2 = relations_table[["Transcription_Factor", "Gene"]].groupby(["Gene"]).Transcription_Factor.count().rename("Transcription_factors_per_gene_count").to_frame().reset_index()
    pipeline.stat(table_stat_2, "Transcription_factors_per_gene_count", "hist, box", "\t", f"{follow_process_dir}/graphics")

    table_stat_3 = relations_table[["Transcription_Factor", "Pattern_TF"]].groupby(["Pattern_TF"]).Transcription_Factor.count().rename("Transcription_factors_per_TF_pattern_count").to_frame().reset_index()
    pipeline.stat(table_stat_3, "Transcription_factors_per_TF_pattern_count", "hist, box", "\t", f"{follow_process_dir}/graphics")

    pipeline.stat(gene_per_gene_pattern_count_table[["Pattern_Gene", "Nb_genes_per_gene_pattern"]], "Nb_genes_per_gene_pattern", "hist, box", "\t", f"{follow_process_dir}/graphics")


    text_info = f"""



## Genes per transcription factors number : 
***

{pipeline.display_table_stat("graphics/Genes_per_TF_count_hist.png", "Genes_per_TF_hist","graphics/Genes_per_TF_count_box.png" , "Genes_per_TF_box", table_stat_1.describe().to_markdown())}

***

## Transcription factors per gene number : 


{pipeline.display_table_stat("graphics/Transcription_factors_per_gene_count_hist.png", "TF_per_gene_hist", "graphics/Transcription_factors_per_gene_count_box.png", "TF_per_gene_box", table_stat_2.describe().to_markdown())}

***

## Transcription factors per transcription factors' pattern count :    


{pipeline.display_table_stat("graphics/Transcription_factors_per_TF_pattern_count_hist.png", "TF_per_TF_pattern_hist","graphics/Transcription_factors_per_TF_pattern_count_box.png" , "TF_per_TF_pattern_box", table_stat_3[["Transcription_factors_per_TF_pattern_count"]].describe().to_markdown())}

***

# Genes per gene pattern's number analysis : 


## Genes per Genes' pattern count :    

{pipeline.display_table_stat("graphics/Nb_genes_per_gene_pattern_hist.png", "Genes_per_Gene_pattern_hist","graphics/Nb_genes_per_gene_pattern_box.png" , "Genes_per_Gene_pattern_box", gene_per_gene_pattern_count_table[["Nb_genes_per_gene_pattern"]].describe().to_markdown())}

***

"""

    with open(follow_process_path, "a") as follow_process_file : 
        follow_process_file.write(text_info)

    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]


# relation_table_analysis("/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/resultSain.csv", "/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/gene_per_pattern_nb/gene_per_pattern_nb_B.csv", "\t", "/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/Follow_process_test.md")

# gene_per_gene_pattern_count_table = pd.read_csv("/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/gene_per_pattern_nb/gene_per_pattern_nb_B.csv", sep = "\t", header = 0)
# stat(gene_per_gene_pattern_count_table[["Pattern_Gene", "Nb_genes_per_gene_pattern"]], "Nb_genes_per_gene_pattern", "hist, box", "\t", f"/home/eveadmin/Bureau/Stage_Eve_2021_nouveau/graphics")


if "__main__" == __name__ :

    parser = argparse.ArgumentParser()

    parser.add_argument("--relation_table_path", help = "Path to the file containing relations' information.")
    parser.add_argument("-gct", "--genes_per_gene_pattern_count_table", help = "Path to the file containing the count of genes per gene's pattern.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")

    args = parser.parse_args()

    relations_table_analysis(args.relation_table_path, args.genes_per_gene_pattern_count_table_path, args.sep_table, args.follow_process_path)