#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description : 

This script provides a hierargical graph of parents MeSH terms of a MeSH terms of interest. 
The goal here is to help the choice of MeSH terms to query with 
(for the count of transcription factor's citations). 


--> still a pb

example for : 
- Cell Differentiation : D002454
- B-Lymphocytes : D001402
- Plasma Cell : D010950
- Transcription Factors : D014157

MeSH endpoint : https://id.nlm.nih.gov/mesh/sparql 
documentation : https://hhs.github.io/meshrdf/sparql-and-uri-requests 

requiers pygraphviz and networkx

matplotlib version : 2.2.3

--> Arguments : 

"-MeSH", "--MeSH_terms" : Labels and MeSH identifers to provide the related graph (ex:B-Lymphocytes:D001402,Cell Differentiation:D002454).

--> Proceeding : 

- searchs in a RDF graph parents and children of a MeSH term of interest (-MeSh), its label, tree number, identifyer
- from first parents, defines all arcs by finding theirs direct children (+ 4 in their tree number)
- do it untill last children (futur parents are all current parents children)
- because labels' nodes overlapping : modifying positions (several tests)
    - for each y nodes positions : add a step 


--> A command line example : 
python graph_MeSH.py -MeSH "B-Lymphocytes:D001402" # -o graph_MeSH.png

'''

from SPARQLWrapper import SPARQLWrapper, JSON
import pandas as pd
import argparse 
import time 
import datetime 

import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout
import os 

from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage



def graph_MeSH(follow_process_path, MeSH_terms, MeSH_graph_dir) :
    '''
    '''

    t0 = time.time()

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module graph_MeSH.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

- MeSH_terms : {MeSH_terms}
- MeSH_graph_dir : {MeSH_graph_dir}
- follow_process_path : {follow_process_path}

""")
    print(f"""

********************************************************************************
--> Module graph_MeSH.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    for MeSH in MeSH_terms.split(", ") :

        # Graph creation  
        
        G = nx.Graph()

        sparql = SPARQLWrapper("http://id.nlm.nih.gov/mesh/sparql")
        sparql.setQuery(""" 

            PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
            PREFIX owl: <http://www.w3.org/2002/07/owl#>
            PREFIX meshv: <http://id.nlm.nih.gov/mesh/vocab#>
            PREFIX mesh: <http://id.nlm.nih.gov/mesh/>
            PREFIX mesh2015: <http://id.nlm.nih.gov/mesh/2015/>
            PREFIX mesh2016: <http://id.nlm.nih.gov/mesh/2016/>
            PREFIX mesh2017: <http://id.nlm.nih.gov/mesh/2017/>

            SELECT DISTINCT ?label ?ancChildTreeNum ?ancChild
            FROM <http://id.nlm.nih.gov/mesh>

            WHERE {
                {
                    mesh:""" + MeSH[-7:] + """ meshv:treeNumber ?treeNum .
                    ?treeNum meshv:parentTreeNumber* ?ancChildTreeNum .
                    ?ancChild meshv:treeNumber ?ancChildTreeNum .
                    ?ancChild rdfs:label ?label
                }
                UNION
                {
                    mesh:""" + MeSH[-7:] + """ meshv:treeNumber ?treeNum .
                    ?ancChildTreeNum meshv:parentTreeNumber* ?treeNum .
                    ?ancChild meshv:treeNumber ?ancChildTreeNum .
                    ?ancChild rdfs:label ?label 
                }
            }

            ORDER BY ?treeNum ?ancestorTreeNum

        """)

        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        # parent_1 --> mesh:A11 len = 8 (avec mesh : http://id.nlm.nih.gov/mesh/, soit len = 27 (+ 8 = 35 ) )
        # parent_2 --> mesh:A11.063 len = 8 + 4
        # parent_3 --> mesh:A11.063.438 len = 8 + 4 + 4...

        dict_nodes = {} 

        for result in results["results"]["bindings"] :
            node_label = result["label"]["value"]
            node_tree_num = result["ancChildTreeNum"]["value"][26:]  # to not conserves "http://id.nlm.nih.gov/mesh/" (len = 27)
            node_id = result["ancChild"]["value"][-7:]  # to not conserves prefix mesh:

            dict_nodes[node_tree_num] = [node_label, len(node_tree_num), node_id]  # 0:label, 1:len, 2:id
            
            # adds all nodes 
            if node_label not in G.nodes() : 
                G.add_node(f"{node_label}\n{node_id}")


        # Class all nodes on their len : degree on the tree
        # tt modulo 4 
        # recursive funtion 

        parents = {k:v for (k,v) in dict_nodes.items() if (v[1] == 4)} # First parents (id as a len of 4)
        all_children = dict_nodes

        pos = {}  # defines nodes positions
        windows_size = 100
        top_y = 100

        while parents != {} :

            parents_nb = len(parents.keys())
            step_x =  windows_size / (parents_nb + 1)
            print(step_x)

            for i_p, parent in enumerate(parents.keys()) : 

                parent_len = dict_nodes[parent][1]

                # is a direct child if ancestor TreeNum is like the beginning of the child one and that the len of this number is + 4
                children = {k:v for (k,v) in dict_nodes.items() if (((k[:parent_len]) == parent) & (v[1] == (parent_len + 4)))} 
                
                # Defines positions :
                if f"{dict_nodes[parent][0]}\n{dict_nodes[parent][2]}" not in pos.keys() :
                    pos[f"{dict_nodes[parent][0]}\n{dict_nodes[parent][2]}"]  = (((i_p +1) * step_x), top_y)
                    # print(f"{dict_nodes[parent][0]}\n{dict_nodes[parent][2]}")

                for child in children.keys() : 
                    # adds edge child + parent
                    G.add_edge(f"{dict_nodes[parent][0]}\n{dict_nodes[parent][2]}", f"{children[child][0]}\n{children[child][2]}")
                    
                # stores all new children to become parents :        
                all_children.update(children)

            parents = all_children
            print(all_children)
            all_children = {}
            top_y -= 10
            

        # Save graph :

        #nx.nx_agraph.write_dot(G,'test.dot')

        plt.title(f"graph_{MeSH[:-8]}")
        # pos = nx.drawing.nx_agraph.graphviz_layout(G, prog = 'dot')

        # # Zoom the graph in horizontal only : (could be possible with polynomes solutions)
        # df_pos = pd.DataFrame({"pos":list(pos.values()), "node":list(pos.keys())})
        # df_pos["x"] = [x for (x,y) in df_pos["pos"]]
        # df_pos["y"] = [y for (x,y) in df_pos["pos"]]

        # # Defines coordinates of the middle point
        # X_mid = df_pos["x"].max()/2
        # #Y_mid = df_pos["y"].min()/2

        # step = 15

        # new_X = []

        # #df_pos = df_pos.sort_values(by = ["y"]) don't care : for all X
        # # must be proportional of the to-middle-distance 

        # for c_x in df_pos["x"] :
        #     if c_x > X_mid : 
        #         new_X.append(round(((1/9) * (2 * c_x)) + X_mid, 2)) # Test with adding the dstance to the middle X
        #     if round(c_x) == round(X_mid) : 
        #         new_X.append(X_mid)
        #     if c_x < X_mid : 
        #         new_X.append(round(((1/9) * (2 * c_x)) + X_mid, 2))

        # df_pos["x_n"] = new_X


        # # to make node in alternance (quinconce)
        # df_pos = df_pos.sort_values(by = ["y", "x_n"], ascending = False)

        # new_Y = []
        # i = 0
        # for c_y in df_pos["y"]: 
        #     if i == 0 : 
        #         new_Y.append(c_y + 2 *step)
        #         i += 1
        #     elif i == 1 : 
        #         new_Y.append(c_y + step)
        #         i += 1
        #     else : 
        #         new_Y.append(c_y - step)
        #         i = 0

        # df_pos["y_n"] = new_Y

        # df_pos["new_pos"] = [(x,y) for (x,y) in zip(df_pos["x_n"], df_pos["y_n"])]

        # new_pos_dic = {k:v for (k,v) in zip(df_pos["node"], df_pos["new_pos"])}

        # df_pos.to_csv(f"graph_{MeSH[:-8]}_coordinates.csv", index = None, sep = ",")

        #plt.show()
        # nx.draw(G, pos = new_pos_dic, with_labels = True, node_color = 'red', arrows = False, node_size = 5, node_shape = "o", font_size = 6)
        nx.draw(G, pos = pos, with_labels = True, node_color = 'red', node_size = 5, node_shape = "o", font_size = 6)
        
        if not os.path.exists(MeSH_graph_dir):
            os.makedirs(MeSH_graph_dir)

        plt.savefig(f"{MeSH_graph_dir}/{MeSH[:-8]}_graph.png")
        plt.close()

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info += f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), 0]

if __name__ == "__main__" : 


    parser = argparse.ArgumentParser()

    parser.add_argument("-MeSH", "--MeSH_terms", help = "Labels and MeSH identifers to provide the related graph (ex:B-Lymphocytes:D001402,Cell Differentiation:D002454).")
    parser.add_argument("-file", "--MeSH_graph_dir", help = "Path to the directory storing MeSH terms graphs.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")

    args = parser.parse_args()

    graph_MeSH(args.follow_process_path, args.MeSH_terms, args.MeSH_graph_dir)