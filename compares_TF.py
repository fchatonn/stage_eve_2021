#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

This scripts compares transcription factors' lists and returns TF that are specific and common to tables.

--> Arguments : 
    "-t1", "--TF_table_1" : Path to the first file containing Transcription factors to compare.
    "-t2", "--TF_table_2" : Path to the second file containing Transcription factors to compare.
    "-n", "--comparison_name" : Name to given to the comparison.
    "-o", "--output_file" : Path to output file containing comparison.

--> Proceeding : 
- imports both tables to compare (t1, t2)
- defines commons transcription factors : labels and count
- defines specific transcriptionf factors in each tables : labels and count
- stores this comparison table to the name given (n) in the file given (o)

A command line to use can be : 
python3 compares_TF.py -t1 TF_tables/TF_table_q75.csv -t2 TF_tables/TF_table_q75_pre_100.csv -n not_vs_del_10
python3 compares_TF.py -t1 TF_tables/TF_table_q75.csv -t2 TF_tables/TF_table_q75_pre_100.csv -n not_vs_del_10

python3 compares_TF.py -t1 resultSain.csv -t2 preprocessed_relations/preprocessed_relations_del_10.csv -n resultSain_vs_del_10
'''

import pandas as pd
import argparse 
import time 
import datetime 
import os
# import nbformat
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage


def compares_TF(follow_process_path, TF_table_1, TF_table_2, comparison_name, output_file, sep_table) : 
    '''
    '''

    t0 = time.time()

    # Checks existence of input files : 
    if not os.path.exists(TF_table_1) :
        raise ValueError(f"{TF_table_1} does not exist. Change input.")
    if not os.path.exists(TF_table_2) :
        raise ValueError(f"{TF_table_2} does not exist. Change input.")
    
   # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module compares_TF.py :</font> 

lauched at {datetime.datetime.today()}

with arguments :

| Arguments | |
| --- | --- |
| follow_process_path | {follow_process_path} |
| TF_table_1 | {TF_table_1} |
| TF_table_2 | {TF_table_2} |
| comparison_name | {comparison_name} |
| output_file | {output_file} |


""")
    print(f"""

********************************************************************************
--> Module compares_TF.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(text_info)

    # Imports data : 
    table_1 = pd.read_csv(TF_table_1, sep = sep_table, header = 0, usecols = ["Transcription_Factor"]).drop_duplicates()
    table_2 = pd.read_csv(TF_table_2, sep = sep_table, header = 0, usecols = ["Transcription_Factor"]).drop_duplicates()

    table_compare = pd.DataFrame({})
    common_TF = table_1.merge(table_2, on = ["Transcription_Factor"], how = "inner")
    table_compare[f"common_TFs_{common_TF.shape[0]}"] = [(", ").join(common_TF["Transcription_Factor"])]


    if not os.path.exists("/".join(output_file.split("/")[:-1])):
        os.makedirs("/".join(output_file.split("/")[:-1]))


    with open(output_file, "a") as file_result :
        file_result.write(f"\n\n# {comparison_name}\n\n")

        list_spe_TF = []
        for TF in table_1["Transcription_Factor"] :
            if TF not in list(common_TF["Transcription_Factor"]) :
                list_spe_TF.append(TF)
        table_compare[f"1_all_{table_1.shape[0]}_spe_{len(list_spe_TF)}"] = [", ".join(list_spe_TF)]

        list_spe_TF = []
        for TF in table_2["Transcription_Factor"] :
            if TF not in list(common_TF["Transcription_Factor"]) :
                list_spe_TF.append(TF)

        table_compare[f"2_all_{table_2.shape[0]}_spe_{len(list_spe_TF)}"] = [", ".join(list_spe_TF)]

        table_compare.to_markdown(file_result)

    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")

    with open(follow_process_path, "a") as info_file:
        # Informations on the srcipt :
        info_file.write(f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    
    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int((os.path.getsize(TF_table_1) + os.path.getsize(TF_table_1)) / 1024)]



if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-t1", "--TF_table_1", help = "Path to the first file containing Transcription factors to compare.")
    parser.add_argument("-t2", "--TF_table_2", help = "Path to the second file containing Transcription factors to compare.")
    parser.add_argument("-n", "--comparison_name", help = "Name to given to the comparison.")
    parser.add_argument("-o", "--output_file", help = "Path to output file containing comparison.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")

    args = parser.parse_args()

    compares_TF(args.follow_process_path, args.TF_table_1, args.TF_table_2, args.comparison_name, args.output_file, args.sep_table)
