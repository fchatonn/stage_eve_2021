#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
@author : Ève Barré

--> General description :

This scripts updates the result table that class transcription factors in funciton of filter pass.

All filters are : 

    - all TF
    - differential expression +/- constant
    - compatibility table 
    - specificity + coverage
    - ? : annotations, publication's citation number ... 

Which presentation is better : keep the order of filter or put in the top best TFs ? 

--> Arguments : 

"-t", "--TF_table" : Path to the new file containing Transcription factors that passed filter.
"-n", "--filter_name" : Filter just applied.")
"-ct", "--class_table" : Path to output file containing the classification table if an ohter filter has already been done.
"-an", "--annotation_table" : Path to the all transcription factor annotated table.
# "-col_cat", "--columns_keywords_categories" : Columns number of keywords categories
"-col_go", "--columns_go_terms" : Columns number of go terms annotations.
"-col_G_cit", "--columns_G_citations" : Columns number of citation's number's annotations (all).
"-col_max_cit", "--column_max_MeSH_citations" : Column id_number of papers number annotated by the maximum and MeSH term wanted.
"-col_min_cit", "--column_min_MeSH_citations" : Column id_number of papers number annotated by the minimum and MeSH term wanted.

--> Proceeding : 

    - imports and merges transcription factors' and annotations' tables
    - counts transcription factors number annotated (go terms and citations number)
    - compares which TF are and aren't passed the previous filter and counts
    - stores the table wih the filter associated new row 

--> A command line to use can be : 
python update_class.py -t ref_valid_tf.csv -n Initial_TFs -an annotated_TF_table/annotation_all_TF_ini.csv -col_go "5,6,7,8,9,10"
python update_class.py -t resultSain.csv -n differential_expression -ct class_table.csv -an annotated_TF_table/annotation_all_TF_ini.csv -col_go "5,6,7,8,9,10"
python update_class.py -t TF_tables/TF_table_q75.csv -n specificity_coverage -ct class_table.csv -an annotated_TF_table/annotation_all_TF_ini.csv -col_go "5,6,7,8,9,10"
'''

import pandas as pd
import argparse 
import time 
import datetime 
import os
import matplotlib.pyplot as plt
from resource import getrusage, RUSAGE_SELF  # to assess the peak memory usage
import ast 

# functions needed : 

def get_annotated_row(list_str_TF, df_TF, go_terms_as_column, citations_count_as_column, annotation_table) :
    '''
    '''

    list_str_TF = list_str_TF.split(", ")

    TF_annotated_table = pd.DataFrame({"Transcription_Factor" : list_str_TF})
    TF_annotated_table = TF_annotated_table.merge(annotation_table, how = "left")

    if go_terms_as_column : 
        for go_term in go_terms_as_column.split(", ") :
            df_TF[go_term] = list(TF_annotated_table[[key for key in annotation_table.columns if go_term in key][0]]).count("X")
        
    if citations_count_as_column : 
        for cit in citations_count_as_column.split(", ") : 
            df_TF[cit] = TF_annotated_table[cit].mean()
        
    return df_TF


def update_class(follow_process_path, TF_table, filter_name, class_table_path, annotation_table, sep_table, go_terms_as_column, citations_count_as_column) :
    '''
    '''

    t0 = time.time()

    # asserts is a .md file 
    if follow_process_path[-3:] != ".md" :
        raise ValueError(f"'follow_process_path' must be a markdown (.md) file not {follow_process_path.split('.')[-1]}.")

    print(f"""

********************************************************************************
--> Module update_class.py :
lauched at {datetime.datetime.today()}
********************************************************************************

""")

    # Checks input files' existance :
    if not os.path.exists(TF_table):
        raise ValueError(f"{TF_table} does not exist. Change input.")
    
    if not os.path.exists(annotation_table):
        raise ValueError(f"{annotation_table} does not exist. Change input.")

    if not os.path.exists("/".join(follow_process_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))
    
    if not os.path.exists("/".join(class_table_path.split("/")[:-1])):
        os.makedirs("/".join(follow_process_path.split("/")[:-1]))

    # markdown file to follow process :
    text_info = (f"""

# <font color=orange>Module update_class.py :</font> 

lauched at {datetime.datetime.today()}

with arguments : 

| Arguments | |
| --- | --- |
| filter_name | {filter_name} |
| TF_table | {TF_table} |
| class_table | {class_table_path} |
| follow_process_path | {follow_process_path} |
| annotation_table | {annotation_table} |
| go_terms_as_column | {go_terms_as_column} |
| citations_count_as_column | {citations_count_as_column} |
| sep_table | {sep_table} |


""")

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    
    input_size = os.path.getsize(TF_table) 
    
    # Imports data : 
    TF_table = pd.read_csv(TF_table, sep = sep_table, header = 0, usecols = ["Transcription_Factor"]).drop_duplicates()

    # if type(dict_annot_columns_id) == str : 
    #     dict_annot_columns_id = ast.literal_eval(dict_annot_columns_id)

    if os.path.exists(class_table_path) : 
        input_size += os.path.getsize(class_table_path) 

        class_table =  pd.read_csv(class_table_path, sep = sep_table, header = 0)

        # Defines with TF just failed to pass filter 

        new_previous_TF_list = ""
        next_TF_list = ""

        for old_TF in class_table["passed_TFs"][0].split(", ") :
            if old_TF in list(TF_table["Transcription_Factor"]) :
                if len(next_TF_list) == 0 : 
                    next_TF_list += old_TF
                else : 
                    next_TF_list += ", " + old_TF
            else : 
                if len(new_previous_TF_list) == 0 :
                    new_previous_TF_list += old_TF
                else  :
                    new_previous_TF_list += ", " + old_TF
        
        previous_row = pd.DataFrame({"filter_name":[class_table["filter_name"][0]], "passed_TFs" : [new_previous_TF_list], "candidate_nb" : [class_table["candidate_nb"][0]]})

    else : 
        next_TF_list = ", ".join(list(TF_table["Transcription_Factor"]))
        new_previous_TF_list = None
    

    new_row = pd.DataFrame({"filter_name":[filter_name], "passed_TFs" : [next_TF_list], "candidate_nb" : [len(next_TF_list.split(', '))]})


    if annotation_table : 
        annotation_table = pd.read_csv(annotation_table, sep = sep_table, header = 0)

        new_row = get_annotated_row(next_TF_list, new_row, go_terms_as_column, citations_count_as_column, annotation_table)

        if os.path.exists(class_table_path) : 
            previous_row = get_annotated_row(new_previous_TF_list, previous_row, go_terms_as_column, citations_count_as_column, annotation_table)


    # modifiying class_table : 

    if not os.path.exists(class_table_path) : 
        class_table = new_row
        new_row.to_csv(class_table_path, sep = sep_table, index = None)

    else : 
        # Replace TF list modified : 
        class_table.loc[0] = previous_row.loc[0]
        new_row.append(class_table, ignore_index = True).to_csv(class_table_path, sep = sep_table,  index = None)


    # Execution time 
    print(f"Ends at {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes")
    text_info = f"\nExecution time : {round((time.time() - t0), 3)} seconds <=> {round(((time.time() - t0)/60), 3)} minutes"

    with open(follow_process_path, "a") as follow_process_file :
        follow_process_file.write(text_info)
    

    # execution_time (sec), execution_time (min), memory_usage (MiB), input_file_size (MiB)
    return [round((time.time() - t0), 3), round(((time.time() - t0)/60), 3), int(getrusage(RUSAGE_SELF).ru_maxrss / 1024), int(input_size / 1024)]



if __name__ == "__main__" : 

    parser = argparse.ArgumentParser()

    parser.add_argument("-t", "--TF_table", help = "Path to the new file containing Transcription factors that passed filter.")
    parser.add_argument("-n", "--filter_name", help = "Filter just applied.")
    parser.add_argument("-ct", "--class_table_path", help = "Path to output file containing the classification table if an ohter filter has already been done.")
    parser.add_argument("--go_terms_as_column", help = "Go terms to keep as column in the classification (gives the annoated TF count).")
    parser.add_argument("--citations_count_as_column", help = "Papers citation count (general, max, min) to keep as column in the classification (gives the annoated TF count).")
    parser.add_argument("--annotation_table", help = "All TF annotations table path.")
    parser.add_argument("--follow_process_path", help = "Path to the file to save process data.")
    parser.add_argument("-s", "--sep_table", help = "String that separates columns in tables.")

    args = parser.parse_args()

    update_class(args.follow_process_path, args.TF_table, args.filter_name, args.class_table_path, args.annotation_table, args.sep_table, args.go_terms_as_column, args.citations_count_as_column)